/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   client.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: garm <garm@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/04/01 15:37:59 by garm              #+#    #+#             */
/*   Updated: 2015/12/01 07:54:42 by garm             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "client.h"

static t_remote	*create_remote(void)
{
	t_remote	*remote;

	if (!(remote = ft_memalloc(sizeof(t_remote))))
		return (NULL);
	return (remote);
}

void			connect_server(t_gui *gui, t_net *net, char *host, int port)
{
	t_com		*server;

	server = network_connect(net, host, port, NULL);
	if (server)
	{
		recv_hook(server, &event_recv);
		ack_hook(server, &event_ack);
		disconnect_hook(server, &event_disconnect);
		network_recv_mode(server, MODE_TEXT);
	}
	else
	{
		show_msg(gui, "Can't connect.", gui->cursor.chan);
		gui_refresh(gui);
	}
}

int				main(int argc, char **argv)
{
	t_net		*net;
	t_gui		*gui;

	net = network_create(&create_remote);
	gui_init((t_remote *)net);
	network_enable_ncurses_callback(net);
	gui = ((t_remote *)net)->gui;
	stdin_hook(net, &event_stdin);
	if (argc == 2)
		connect_server(gui, net, argv[1], DEFAULT_PORT);
	else if (argc > 2)
		connect_server(gui, net, argv[1], ft_atoi(argv[2]));
	wmove(gui->prompt, 1, 2);
	wrefresh(gui->prompt);
	network_loop(net);
	gui_end(gui);
	return (0);
}
