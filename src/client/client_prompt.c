/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   client_prompt.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: garm <garm@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/10/07 15:37:34 by garm              #+#    #+#             */
/*   Updated: 2015/11/28 01:51:06 by garm             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "client.h"

WINDOW		*create_prompt(void)
{
	WINDOW	*win;

	win = newwin(3, COLS, ABS(LINES - 3), 0);
	box(win, 0, 0);
	wrefresh(win);
	return (win);
}

void		destroy_prompt(WINDOW *win)
{
	wborder(win, ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ');
	wrefresh(win);
	delwin(win);
}

WINDOW		*clear_prompt(WINDOW *win)
{
	wclear(win);
	destroy_prompt(win);
	return (create_prompt());
}

void		prompt_refresh(t_gui *gui)
{
	wclear(gui->prompt);
	box(gui->prompt, 0, 0);
	if (gui->cursor.chan && gui->cursor.chan != gui->empty_chan)
	{
		mvwaddstr(gui->prompt, 0, 2, " ");
		waddstr(gui->prompt, gui->cursor.chan->name);
		waddstr(gui->prompt, " ");
	}
	line_draw(gui->line, gui->prompt);
	wrefresh(gui->prompt);
}
