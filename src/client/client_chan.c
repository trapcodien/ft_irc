/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   client_chan.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: garm <garm@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/01 07:31:56 by garm              #+#    #+#             */
/*   Updated: 2015/12/01 07:34:09 by garm             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "client.h"

t_chan		*create_chan(char *name)
{
	t_chan	*c;

	c = ft_memalloc(sizeof(t_chan));
	c->name = ft_strdup(name);
	c->messages = ft_lsd_create();
	return (c);
}

void		destroy_chan(t_chan *c)
{
	if (!c)
		return ;
	if (c->name)
		ft_strdel(&c->name);
	destroy_messages_list(c->messages);
	ft_memdel((void **)&c);
}

void		destroy_channels_list(t_lsd *channels)
{
	t_chan	*c;

	while ((c = ft_lsd_popback(channels)))
		destroy_chan(c);
	ft_lsd_destroy(&channels);
}
