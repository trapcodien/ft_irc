/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   client_msg.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: garm <garm@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/01 07:30:38 by garm              #+#    #+#             */
/*   Updated: 2015/12/01 07:31:42 by garm             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "client.h"

t_msg		*create_msg(char *name, char *content)
{
	t_msg	*m;

	if (!(m = ft_memalloc(sizeof(t_msg))))
		return (NULL);
	m->name = name;
	m->content = content;
	m->length = ft_strlen(content);
	m->colors.title = DEFAULT_COLOR;
	m->colors.content = DEFAULT_COLOR;
	return (m);
}

void		destroy_msg(t_msg *m)
{
	if (m)
	{
		ft_strdel(&m->name);
		ft_strdel(&m->content);
		ft_memdel((void **)&m);
	}
}

void		destroy_messages_list(t_lsd *l)
{
	t_msg	*m;

	while ((m = ft_lsd_popback(l)))
		destroy_msg(m);
	ft_lsd_destroy(&l);
}
