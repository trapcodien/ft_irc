/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   client_keys.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: garm <garm@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/10/07 16:38:42 by garm              #+#    #+#             */
/*   Updated: 2015/10/10 15:06:32 by garm             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "client.h"

t_bool		line_left(t_line *l)
{
	int		v;

	if (l->cursor == l->buf)
		return (FALSE);
	l->cursor--;
	if (l->cursor < l->start)
	{
		if (COLS / 2 < l->start - l->buf)
			v = COLS / 2;
		else
			v = l->start - l->buf;
		l->start -= v;
		l->end -= v;
	}
	return (TRUE);
}

void		line_right(t_line *l)
{
	int		v;

	if (l->cursor == l->buf_end)
		return ;
	l->cursor++;
	if (l->cursor > l->end)
	{
		if (COLS / 2 < l->buf_end - l->end)
			v = COLS / 2;
		else
			v = l->buf_end - l->end;
		l->start += v;
		l->end += v;
	}
}

void		line_delete(t_line *l)
{
	if (l->cursor == l->buf_end)
		return ;
	if (l->cursor + 1 != l->buf_end)
		ft_memmove(l->cursor, l->cursor + 1, l->buf_end - l->cursor);
	l->buf_end--;
	l->buf_end[0] = 0;
}

void		line_backdelete(t_line *l)
{
	if (line_left(l))
		line_delete(l);
}
