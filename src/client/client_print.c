/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   client_print.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: garm <garm@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/01 07:40:53 by garm              #+#    #+#             */
/*   Updated: 2015/12/01 07:56:09 by garm             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "client.h"

static char		*client_fill(WINDOW *chat, char *msg, t_position *pos, t_msg *m)
{
	size_t		msglen;
	char		*ret;

	msglen = ft_strlen(msg);
	if ((long)msglen > COLS - pos->cols)
	{
		msglen = COLS - pos->cols;
		ret = msg + msglen;
	}
	else
		ret = NULL;
	COLOR_ON(m->colors.content, chat);
	mvwaddnstr(chat, pos->lines, pos->cols - 2, msg, (int)msglen);
	COLOR_OFF(m->colors.content, chat);
	pos->cols = COLUMNS_ALIGN;
	return (ret);
	(void)m;
}

static char		*fill_name(WINDOW *chat, t_position *pos, t_msg *m)
{
	size_t		len;

	if (m->name)
	{
		len = ft_strlen(m->name);
		COLOR_ON(m->colors.title, chat);
		mvwaddnstr(chat, pos->lines, 2, m->name, len);
		COLOR_OFF(m->colors.title, chat);
		waddnstr(chat, " : ", 3);
		pos->cols += len + 3;
	}
	return (m->content);
	(void)chat;
	(void)pos;
}

void			chatbox_print(WINDOW *chat, t_lsd *messages, t_cursor *cursor)
{
	t_msg		*m;
	char		*ptr;
	t_position	pos;

	m = (cursor->msg) ? (cursor->msg) : (messages->head);
	pos.lines = 1;
	pos.cols = COLUMNS_ALIGN;
	while (m && pos.lines < LINES - 4)
	{
		ptr = fill_name(chat, &pos, m);
		while (pos.lines < LINES - 4 && (ptr = client_fill(chat, ptr, &pos, m)))
			pos.lines++;
		if (pos.lines < LINES - 4)
			m = (t_msg *)m->priv.next;
		pos.lines++;
	}
	if (pos.lines >= LINES - 4)
		cursor->gui_is_full = TRUE;
	cursor->at_end = (m) ? FALSE : TRUE;
}
