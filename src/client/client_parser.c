/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   client_parser.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: garm <garm@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/01 07:23:40 by garm              #+#    #+#             */
/*   Updated: 2015/12/01 08:24:27 by garm             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "client.h"

static void	client_who(t_gui *gui, t_res *res)
{
	if (ft_strequ(res->name, ""))
		show_who_msg(gui, "Anonymous", gui->cursor.chan);
	else
		show_who_msg(gui, res->name, gui->cursor.chan);
}

static void	client_mp(t_gui *gui, t_res *res, char *buffer)
{
	buffer[ft_strlen(buffer) - 1] = '\0';
	show_pm(gui, gui->cursor.chan, res->name, res->msg);
}

static void	client_newnick(t_gui *gui, t_res *res)
{
	ft_memcpy(gui->nickname, res->name, USER_MAX_LENGTH);
	show_msg(gui, "Nickname successfully changed", gui->cursor.chan);
}

static void	client_otherwise(t_gui *gui, char *buffer)
{
	buffer[ft_strlen(buffer) - 1] = '\0';
	show_msg(gui, buffer + 2, gui->cursor.chan);
}

void		parse_res(t_gui *gui, char *buf, t_res *r)
{
	if (ft_sscanf(buf, "S:%s:who:%s\n", &r->chan, &r->name) == 2)
		client_who(gui, r);
	else if (ft_sscanf(buf, F_ISNOW, &r->chan, &r->old_name, &r->name) == 3)
		show_msg(gui, buf + 3 + ft_strlen(r->chan), GCBN(gui, r->chan));
	else if (ft_sscanf(buf, "S:%s:joined\n", &r->chan) == 1)
		client_joined(gui, r);
	else if (ft_sscanf(buf, "S:%s:leaved\n", &r->chan) == 1)
		client_leaved(gui, r);
	else if (ft_sscanf(buf, "S:%s:%s join\n", &r->chan, &r->name) == 2)
		client_join(gui, r);
	else if (ft_sscanf(buf, "S:%s:%s leave\n", &r->chan, &r->name) == 2)
		client_leave(gui, r);
	else if (ft_sscanf(buf, "C:%s:%s:%s\n", &r->chan, &r->name, &r->msg) == 3)
		client_message(gui, r);
	else if (ft_sscanf(buf, "M:%s:%s\n", &r->name, &r->msg) == 2)
		client_mp(gui, r, buf);
	else if (ft_sscanf(buf, "newNick:%s\n", &r->name) == 1)
		client_newnick(gui, r);
	else if (ft_sscanf(buf, "msgServer:%s\n", &r->msg) == 1)
		show_msg(gui, r->msg, gui->cursor.chan);
	else
		client_otherwise(gui, buf);
}
