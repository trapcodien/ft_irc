/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   client_event.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: garm <garm@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/01 07:51:35 by garm              #+#    #+#             */
/*   Updated: 2015/12/01 07:54:41 by garm             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "client.h"

void		event_stdin(t_remote *net, int key)
{
	gui_process_key(net->gui, key, (char *)&key);
	prompt_refresh(net->gui);
}

size_t		event_recv(t_com *server, t_cbuf *cbuf, size_t size)
{
	t_gui	*gui;
	char	buffer[BUFF_SIZE];
	t_res	res;

	ft_bzero(buffer, BUFF_SIZE);
	gui = ((t_remote *)server->port->net)->gui;
	ft_cbuf_consume(cbuf, buffer, size);
	if (size > 0)
		parse_res(gui, buffer, &res);
	gui_refresh(gui);
	return (0);
}

void		event_ack(t_com *com, void *data, size_t size)
{
	free(data);
	(void)size;
	(void)com;
}

void		event_disconnect(t_com *com)
{
	t_gui	*gui;

	gui = ((t_remote *)com->port->net)->gui;
	destroy_channels_list(gui->channels);
	gui->channels = ft_lsd_create();
	gui->cursor.chan = gui->empty_chan;
	show_msg(gui, "Disconnected.", gui->cursor.chan);
	gui_refresh(gui);
}
