/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   client_show.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: garm <garm@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/01 07:25:35 by garm              #+#    #+#             */
/*   Updated: 2015/12/01 08:51:50 by garm             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "client.h"

t_msg		*show_msg(t_gui *gui, char *msg, t_chan *c)
{
	char	*formatted_msg;
	t_msg	*m;

	formatted_msg = NULL;
	ft_asprintf(&formatted_msg, "%s", msg);
	m = create_msg(ft_strdup("Server"), formatted_msg);
	m->colors.title = SERV_COLOR;
	m->colors.content = SERV_GREEN_COLOR;
	ft_lsd_pushback(c->messages, m);
	chatbox_end(gui);
	return (m);
}

t_msg		*show_who_msg(t_gui *gui, char *name, t_chan *c)
{
	t_msg	*m;

	m = create_msg(ft_strjoin(c->name, " ->"), ft_strdup(name));
	m->colors.title = SERV_GREEN_COLOR;
	m->colors.content = SERV_GREEN_COLOR;
	ft_lsd_pushback(c->messages, m);
	chatbox_end(gui);
	return (m);
}

t_msg		*show_pm(t_gui *gui, t_chan *c, char *name, char *msg)
{
	char	*formatted_name;
	t_msg	*m;

	formatted_name = NULL;
	ft_asprintf(&formatted_name, "[PM] %s", name);
	m = create_msg(formatted_name, ft_strdup(msg));
	if (ft_strequ(name, gui->nickname))
		m->colors.title = ME_COLOR;
	else
		m->colors.title = PRIVATE_TITLE_COLOR;
	m->colors.content = PRIVATE_MSG_COLOR;
	ft_lsd_pushback(c->messages, m);
	chatbox_end(gui);
	return (m);
}

t_msg		*show_client_msg(t_gui *gui, t_res *res, t_chan *c)
{
	t_msg	*m;

	m = create_msg(ft_strdup(res->name), ft_strdup(res->msg));
	if (ft_strequ(res->name, gui->nickname))
		m->colors.title = ME_COLOR;
	else
		m->colors.title = CLIENT_COLOR;
	ft_lsd_pushback(c->messages, m);
	chatbox_end(gui);
	return (m);
}

t_msg		*show_leave_msg(t_gui *gui, t_res *res, t_chan *c)
{
	char	*formatted_msg;
	t_msg	*m;

	formatted_msg = NULL;
	ft_asprintf(&formatted_msg, "%s leaved.", res->chan);
	m = create_msg(NULL, formatted_msg);
	m->colors.content = SERV_GREEN_COLOR;
	ft_lsd_pushback(c->messages, m);
	chatbox_end(gui);
	return (m);
}
