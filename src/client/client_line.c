/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   client_line.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: garm <garm@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/10/07 16:31:46 by garm              #+#    #+#             */
/*   Updated: 2015/11/28 09:00:26 by garm             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "client.h"

t_line		*line_init(t_line *line)
{
	if (line)
	{
		ft_bzero(line->buf, LINE_SIZE);
		line->buf_end = line->buf;
		line->cursor = line->buf_end;
		line->start = line->cursor;
		line->end = line->start + COLS - 4;
	}
	return (line);
}

t_bool		line_resize(t_line *line)
{
	if (line->end - line->start != COLS - 4)
	{
		line->end = line->start + COLS - 4;
		if (line->cursor > line->end)
		{
			line->start += line->cursor - line->end;
			line->end += line->cursor - line->end;
		}
		move(LINES - 2, line->cursor - line->start + 2);
		return (TRUE);
	}
	return (FALSE);
}

void		line_draw(t_line *line, WINDOW *win)
{
	char	temp;
	size_t	end;

	init_pair(1, COLOR_GREEN, COLOR_BLACK);
	end = line->end - line->start;
	temp = line->start[end];
	line->start[end] = 0;
	mvwprintw(win, 1, 2, line->start);
	line->start[end] = temp;
	if (line->start != line->buf)
	{
		wattron(win, COLOR_PAIR(1));
		mvwaddch(win, 1, 1, '[');
		wattroff(win, COLOR_PAIR(1));
	}
	if (line->buf_end > line->end)
	{
		wattron(win, COLOR_PAIR(1));
		mvwaddch(win, 1, COLS - 2, ']');
		wattroff(win, COLOR_PAIR(1));
	}
	wmove(win, 1, line->cursor - line->start + 2);
}

t_bool		line_addch(t_line *l, char ch)
{
	if (!ft_isprint(ch))
		return (FALSE);
	if (l->buf_end - l->buf >= LINE_SIZE)
		return (FALSE);
	if (l->cursor < l->buf_end)
		ft_memmove(l->cursor + 1, l->cursor, l->buf_end - l->cursor);
	l->cursor[0] = ch;
	l->cursor++;
	l->buf_end++;
	if (l->cursor > l->end)
	{
		l->start++;
		l->end++;
	}
	return (TRUE);
}
