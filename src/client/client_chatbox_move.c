/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   client_chatbox_move.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: garm <garm@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/01 07:34:13 by garm              #+#    #+#             */
/*   Updated: 2015/12/01 07:55:11 by garm             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "client.h"

static int		msg_get_nlines(t_msg *m)
{
	return ((m) ? (((m->length - 1) / (COLS - 4)) + 1) : (0));
}

static t_msg	*chatbox_x_next(t_msg *m, int n)
{
	int		i;

	i = 0;
	while (i < n)
	{
		if (m->priv.next)
			m = (t_msg *)m->priv.next;
		i++;
	}
	return (m);
}

void			chatbox_up(t_gui *gui, t_lsd *messages, t_cursor *c)
{
	if (!c->msg)
		c->msg = messages->head;
	if (c->msg && c->msg->priv.prev)
		c->msg = (t_msg *)c->msg->priv.prev;
	gui_refresh(gui);
}

void			chatbox_down(t_gui *gui, t_lsd *messages, t_cursor *c)
{
	if (!c->at_end)
	{
		if (!c->msg)
			c->msg = messages->head;
		if (c->msg && c->msg->priv.next)
			c->msg = (t_msg *)c->msg->priv.next;
		gui_refresh(gui);
	}
}

void			chatbox_end(t_gui *gui)
{
	t_msg		*m;
	int			limit;
	int			n;

	if (!gui->cursor.chan)
		return ;
	m = gui->cursor.chan->messages->tail;
	limit = LINES - 6;
	n = 0;
	while (m && m->priv.prev && n < limit)
	{
		n += msg_get_nlines(m);
		m = (t_msg *)m->priv.prev;
	}
	if (n > limit || msg_get_nlines(m) > 1)
		m = (t_msg *)m->priv.next;
	if (msg_get_nlines(m) > 1)
		m = chatbox_x_next(m, msg_get_nlines(m) - 1);
	gui->cursor.msg = m;
	gui->cursor.at_end = TRUE;
}
