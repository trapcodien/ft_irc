/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   client_parser_extra.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: garm <garm@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/01 08:04:34 by garm              #+#    #+#             */
/*   Updated: 2015/12/01 08:24:31 by garm             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "client.h"

void			client_joined(t_gui *gui, t_res *res)
{
	t_chan		*joined_channel;

	if (!(joined_channel = get_chan_by_name(gui, res->chan)))
	{
		joined_channel = create_chan(res->chan);
		ft_lsd_pushback(gui->channels, joined_channel);
	}
	gui->cursor.chan = joined_channel;
	destroy_chan(gui->empty_chan);
	gui->empty_chan = create_chan("Empty channel");
	chatbox_end(gui);
}

void			client_leaved(t_gui *gui, t_res *res)
{
	t_chan		*leaved_chan;
	t_chan		*tmp;

	if ((leaved_chan = get_chan_by_name(gui, res->chan)) != NULL)
	{
		if (gui->cursor.chan == leaved_chan)
		{
			if (gui->cursor.chan->prev)
				gui->cursor.chan = gui->cursor.chan->prev;
			else if (gui->cursor.chan->next)
				gui->cursor.chan = gui->cursor.chan->next;
			else
				gui->cursor.chan = gui->empty_chan;
		}
		tmp = leaved_chan;
		ft_lsd_pick(gui->channels, &leaved_chan);
		destroy_chan(tmp);
	}
	show_leave_msg(gui, res, gui->cursor.chan);
	chatbox_end(gui);
}

void			client_join(t_gui *gui, t_res *res)
{
	t_chan		*joined_channel;
	char		*str;

	if ((joined_channel = get_chan_by_name(gui, res->chan)))
	{
		str = ft_strjoin(res->name, " join.");
		show_msg(gui, str, joined_channel);
		ft_strdel(&str);
	}
}

void			client_leave(t_gui *gui, t_res *res)
{
	t_chan		*joined_channel;
	char		*str;

	if ((joined_channel = get_chan_by_name(gui, res->chan)))
	{
		str = ft_strjoin(res->name, " leave.");
		show_msg(gui, str, joined_channel);
		ft_strdel(&str);
	}
}

void			client_message(t_gui *gui, t_res *res)
{
	t_chan		*joined_channel;

	if ((joined_channel = get_chan_by_name(gui, res->chan)))
		show_client_msg(gui, res, joined_channel);
}
