/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   client_chatbox.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: garm <garm@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/10/10 14:58:08 by garm              #+#    #+#             */
/*   Updated: 2015/10/10 15:06:33 by garm             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "client.h"

WINDOW		*create_chatbox(void)
{
	WINDOW	*win;

	win = newwin(LINES - 3, COLS, 0, 0);
	box(win, 0, 0);
	wrefresh(win);
	return (win);
}

void		destroy_chatbox(WINDOW *win)
{
	wborder(win, ' ', ' ', ' ', ' ', ' ', ' ', ' ', ' ');
	wrefresh(win);
	delwin(win);
}

WINDOW		*clear_chatbox(WINDOW *win)
{
	wclear(win);
	destroy_chatbox(win);
	return (create_chatbox());
}
