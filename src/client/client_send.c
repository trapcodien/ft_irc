/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   client_send.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: garm <garm@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/01 07:49:01 by garm              #+#    #+#             */
/*   Updated: 2015/12/01 07:54:59 by garm             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "client.h"

char		*extract_line(t_gui *gui, size_t *size_ptr)
{
	size_t	size;
	char	*line;

	size = gui->line->buf_end - gui->line->buf + 1;
	if (size_ptr)
		*size_ptr = size;
	if (size <= 1)
		return (NULL);
	if (!(line = ft_strnew(size)))
		return (line);
	line[size - 1] = '\n';
	ft_memcpy(line, gui->line->buf, size - 1);
	return (line);
}

int			parse_unconnected_cmd(t_gui *gui, char *cmd)
{
	char	hostname[BUFF_SIZE + 1];
	int		port;
	int		ret;

	ret = 0;
	if (ft_strequ("/exit", cmd))
	{
		show_who_msg(gui, "Good Bye.", gui->cursor.chan);
		network_exit((t_net *)gui->remote);
		ret = 1;
	}
	else if ((ft_sscanf(cmd, "/connect %s %i", &hostname, &port)) == 2)
	{
		network_closeall(gui->remote->priv.connections);
		connect_server(gui, &gui->remote->priv, hostname, port);
		ret = 1;
	}
	else if (ft_strequ("/close", cmd))
	{
		network_closeall(gui->remote->priv.connections);
		ret = 1;
	}
	ft_strdel(&cmd);
	return (ret);
}

void		send_msg(t_gui *gui)
{
	t_com	*serv;
	char	*to_send;
	size_t	size;

	if (parse_unconnected_cmd(gui, ft_strtrim(gui->line->buf)))
	{
		line_init(gui->line);
		return (gui_refresh(gui));
	}
	if (!gui->remote->priv.connections)
	{
		if (*gui->line->buf)
			show_who_msg(gui, "You are not connected, use /exit \
or /connect <host> <port>", gui->cursor.chan);
		line_init(gui->line);
		return (gui_refresh(gui));
	}
	serv = gui->remote->priv.connections->clients;
	to_send = extract_line(gui, &size);
	network_sendack(serv, serv, to_send, size);
	line_init(gui->line);
}
