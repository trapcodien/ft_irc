/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   client_gui.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: garm <garm@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/01 07:38:23 by garm              #+#    #+#             */
/*   Updated: 2015/12/01 07:40:47 by garm             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "client.h"

static void	init_colors(void)
{
	start_color();
	init_pair(SERV_GREEN_COLOR, COLOR_GREEN, COLOR_BLACK);
	init_pair(DEFAULT_COLOR, COLOR_WHITE, COLOR_BLACK);
	init_pair(SERV_COLOR, COLOR_BLACK, COLOR_RED);
	init_pair(CLIENT_COLOR, COLOR_BLACK, COLOR_YELLOW);
	init_pair(PRIVATE_TITLE_COLOR, COLOR_WHITE, COLOR_MAGENTA);
	init_pair(PRIVATE_MSG_COLOR, COLOR_BLACK, COLOR_WHITE);
	init_pair(ME_COLOR, COLOR_WHITE, COLOR_BLUE);
}

void		gui_init(t_remote *remote)
{
	t_gui	*gui;

	initscr();
	halfdelay(100);
	keypad(stdscr, TRUE);
	noecho();
	refresh();
	gui = NULL;
	if (!(gui = ft_memalloc(sizeof(t_gui))))
		return ;
	gui->prompt = create_prompt();
	gui->chatbox = create_chatbox();
	gui->line = line_init(ft_memalloc(sizeof(t_line)));
	gui->cursor.msg = NULL;
	gui->cursor.pos = 0;
	gui->cursor.at_end = TRUE;
	gui->cursor.gui_is_full = FALSE;
	gui->channels = ft_lsd_create();
	gui->empty_chan = create_chan("Infos");
	gui->cursor.chan = gui->empty_chan;
	gui->remote = remote;
	ft_bzero(gui->nickname, USER_MAX_LENGTH + 1);
	remote->gui = gui;
	init_colors();
}

void		gui_end(t_gui *gui)
{
	destroy_prompt(gui->prompt);
	destroy_chatbox(gui->chatbox);
	ft_memdel((void **)&(gui->line));
	destroy_channels_list(gui->channels);
	destroy_chan(gui->empty_chan);
	endwin();
	ft_memdel((void **)&gui);
}

void		gui_refresh(t_gui *gui)
{
	wclear(gui->chatbox);
	box(gui->chatbox, 0, 0);
	chatbox_print(gui->chatbox, gui->cursor.chan->messages, &gui->cursor);
	wclear(gui->prompt);
	box(gui->prompt, 0, 0);
	if (gui->cursor.chan && gui->cursor.chan != gui->empty_chan)
	{
		mvwaddstr(gui->prompt, 0, 2, " ");
		waddstr(gui->prompt, gui->cursor.chan->name);
		waddstr(gui->prompt, " ");
	}
	line_draw(gui->line, gui->prompt);
	wrefresh(gui->chatbox);
	wrefresh(gui->prompt);
}
