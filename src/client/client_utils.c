/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   client_utils.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: garm <garm@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/01 07:45:05 by garm              #+#    #+#             */
/*   Updated: 2015/12/01 07:56:37 by garm             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "client.h"

static void	gui_resize(t_gui *gui)
{
	chatbox_end(gui);
	gui->prompt = clear_prompt(gui->prompt);
	gui->chatbox = clear_chatbox(gui->chatbox);
	line_resize(gui->line);
	gui_refresh(gui);
}

void		channel_switch(t_gui *gui)
{
	t_chan	*chan;
	t_com	*com;

	if (!gui->cursor.chan || gui->cursor.chan == gui->empty_chan)
		return ;
	if (gui->cursor.chan->next)
		chan = gui->cursor.chan->next;
	else
		chan = (t_chan *)gui->channels->head;
	if (chan && (chan->next || chan->prev))
	{
		com = gui->remote->priv.connections->clients;
		network_send(com, com, "/join ", 6);
		network_send(com, com, chan->name, ft_strlen(chan->name));
		network_send(com, com, "\n", 1);
	}
}

void		gui_process_key(t_gui *gui, int key, char *c)
{
	if (key == EVENT_RESIZE)
		gui_resize(gui);
	else if (c[1] == 0 && c[2] == 0 && c[3] == 0 && c[0] == '\n')
		send_msg(gui);
	else if (key == KEY_RETURN)
		line_backdelete(gui->line);
	else if (key == KEY_DELETE)
		line_delete(gui->line);
	else if (key == KEY_TAB)
		channel_switch(gui);
	else if (c[1] == 0 && c[2] == 0 && c[3] == 0 && key != NOTHING)
		line_addch(gui->line, c[0]);
	else if (key == KEY_LEFT)
		line_left(gui->line);
	else if (key == KEY_RIGHT)
		line_right(gui->line);
	else if (key == KEY_UP)
		chatbox_up(gui, gui->cursor.chan->messages, &gui->cursor);
	else if (key == KEY_DOWN)
		chatbox_down(gui, gui->cursor.chan->messages, &gui->cursor);
}

t_chan		*get_chan_by_name(t_gui *gui, char *name)
{
	t_chan	*cursor;

	cursor = (t_chan *)gui->channels->head;
	while (cursor)
	{
		if (ft_strequ(name, cursor->name))
			return (cursor);
		cursor = cursor->next;
	}
	return (NULL);
}
