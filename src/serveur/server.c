/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   server.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: garm <garm@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/04/01 15:39:11 by garm              #+#    #+#             */
/*   Updated: 2015/12/01 06:24:47 by garm             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "server.h"

static t_irc		*ft_create_irc(void)
{
	return ((t_irc *)ft_memalloc(sizeof(t_irc)));
}

static t_client		*ft_create_client(void)
{
	return ((t_client *)ft_memalloc(sizeof(t_client)));
}

static void			event_close_irc(t_irc *server)
{
	if (server->verbose)
		ft_putendl("Bye Bye.");
	destroy_all_channels(server);
	while (server->connected)
		ft_stack_pop(&server->connected, NULL);
}

int					main(int argc, char **argv)
{
	t_net	*net;
	t_port	*server;
	int		verbose;
	int		ret;

	if (argc < 2)
		return (ft_usage(argv[0]));
	verbose = 0;
	if (ft_strequ("-v", argv[1]) || ft_strequ("--verbose", argv[1]))
	{
		verbose = 1;
		argv++;
	}
	net = network_create(NULL);
	stdin_hook(net, &event_stdin);
	server = network_listen(net, ft_atoi(argv[1]), 42, &ft_create_irc);
	if ((ret = check_error(net, server)))
		return (ret);
	if (verbose)
		((t_irc *)server)->verbose = TRUE;
	connect_hook(server, &event_connect, &ft_create_client);
	closeport_hook(server, &event_close_irc);
	network_loop(net);
	return (0);
}
