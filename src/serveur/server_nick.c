/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   server_nick.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: garm <garm@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/04/04 01:46:17 by garm              #+#    #+#             */
/*   Updated: 2015/11/28 02:56:08 by garm             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "server.h"

static int	nick_search(t_client *c, char *nickname)
{
	t_client	*cursor;
	t_port		*port;

	port = c->priv.port;
	cursor = (t_client *)port->clients;
	while (cursor)
	{
		if (cursor != c && ft_strequ(cursor->nick, nickname))
			return (1);
		cursor = (t_client *)cursor->priv.next;
	}
	return (0);
}

static int	is_in_chan(t_client *client, t_chan *channel)
{
	t_irc		*server;
	t_connected	*cursor;

	server = (t_irc *)client->priv.port;
	cursor = server->connected;
	while (cursor)
	{
		if (cursor->channel == channel && cursor->client == client)
			return (1);
		cursor = cursor->next;
	}
	return (0);
}

static void	irc_new_nick(t_client *c, t_connected *cursor, char *new)
{
	t_chan	*chan;
	size_t	size;

	chan = cursor->channel;
	network_send(&c->priv, &cursor->client->priv, "S:", 2);
	size = ft_strlen(chan->name);
	network_send(&c->priv, &cursor->client->priv, chan->name, size);
	network_send(&c->priv, &cursor->client->priv, ":", 1);
	irc_send_dup(c, cursor->client, c->nick);
	network_send(&c->priv, &cursor->client->priv, " is now ", 8);
	irc_send_dup(c, cursor->client, new);
	network_send(&c->priv, &cursor->client->priv, "\n", 1);
}

static void	irc_nick_msg(t_client *c, char *new)
{
	t_irc		*server;
	t_connected	*cursor;

	if (ft_strequ(c->nick, ""))
	{
		ft_strcpy(c->nick, new);
		return (irc_join_broadcast(c));
	}
	server = (t_irc *)c->priv.port;
	cursor = server->connected;
	while (cursor)
	{
		if (cursor->client != c && is_in_chan(c, cursor->channel))
			irc_new_nick(c, cursor, new);
		cursor = cursor->next;
	}
}

void		cmd_nick(t_client *c, char *nickname)
{
	size_t	len;

	len = 0;
	while (nickname[len])
	{
		if (!ft_isalnum(nickname[len]) && nickname[len] != '_')
			return (irc_msg(c, "bad nickname"));
		len++;
	}
	if (len <= 1)
		irc_msg(c, "nickname too short");
	else if (len > USER_MAX_LENGTH)
		irc_msg(c, "nickname too long");
	else if (nick_search(c, nickname))
		irc_msg(c, "nickname already taken");
	else
	{
		irc_msg_newnick(c, nickname);
		if (ft_strnequ(c->nick, nickname))
			irc_nick_msg(c, nickname);
		ft_strcpy(c->nick, nickname);
	}
}
