/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   server_msg.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: garm <garm@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/04/04 03:01:22 by garm              #+#    #+#             */
/*   Updated: 2015/11/28 05:11:54 by garm             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "server.h"

void			irc_msg(t_client *c, char *msg)
{
	network_send(&c->priv, &c->priv, "S:", 2);
	network_send(&c->priv, &c->priv, msg, ft_strlen(msg));
	network_send(&c->priv, &c->priv, "\n", 1);
}

static void		irc_private_msg(t_client *e, t_client *r, char *nick, char *msg)
{
	network_send(&e->priv, &r->priv, "M:", 2);
	network_send(&e->priv, &r->priv, nick, ft_strlen(nick));
	network_send(&e->priv, &r->priv, ":", 1);
	network_sendack(&e->priv, &r->priv, msg, ft_strlen(msg));
	network_send(&e->priv, &r->priv, "\n", 1);
}

void			cmd_msg(t_client *c, char *target, char *msg)
{
	t_client	*cursor;

	cursor = (t_client *)c->priv.port->clients;
	if (!c->nick[0])
		return (irc_msg(c, "no nick"));
	while (cursor)
	{
		if (ft_strequ(target, cursor->nick))
		{
			irc_private_msg(c, cursor, c->nick, ft_strdup(msg));
			return (irc_private_msg(c, c, c->nick, ft_strdup(msg)));
		}
		cursor = (t_client *)cursor->priv.next;
	}
	irc_msg(c, "nick not found");
}

void			irc_join_confirmation(t_client *c, char *chan_name)
{
	char		*cn;

	cn = ft_strdup(chan_name);
	network_send(&c->priv, &c->priv, "S:", 2);
	network_sendack(&c->priv, &c->priv, cn, ft_strlen(chan_name));
	network_send(&c->priv, &c->priv, ":", 1);
	network_send(&c->priv, &c->priv, "joined", 6);
	network_send(&c->priv, &c->priv, "\n", 1);
}

void			irc_leave_confirmation(t_client *c, char *chan_name)
{
	char		*cn;

	cn = ft_strdup(chan_name);
	network_send(&c->priv, &c->priv, "S:", 2);
	network_sendack(&c->priv, &c->priv, cn, ft_strlen(chan_name));
	network_send(&c->priv, &c->priv, ":", 1);
	network_send(&c->priv, &c->priv, "leaved", 6);
	network_send(&c->priv, &c->priv, "\n", 1);
}
