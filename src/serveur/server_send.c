/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   server_send.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: garm <garm@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/28 01:02:08 by garm              #+#    #+#             */
/*   Updated: 2015/11/28 04:03:14 by garm             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "server.h"

void		irc_send_dup(t_client *e, t_client *r, char *data)
{
	size_t	len;
	char	*dup;

	if (!data)
		return ;
	len = ft_strlen(data);
	if (!len)
		return ;
	dup = ft_strdup(data);
	network_sendack(&e->priv, &r->priv, dup, len);
}

static int	irc_check_nick_and_chan(t_client *c)
{
	if (ft_strequ(c->nick, ""))
	{
		irc_msg(c, "no nick");
		return (1);
	}
	else if (!c->current_chan)
	{
		irc_msg(c, "no chan");
		return (1);
	}
	return (0);
}

static void	irc_send_msg(t_client *c, t_cbuff *cbuf, size_t size, t_com *com)
{
	size_t	len_chan;

	len_chan = ft_strlen(c->current_chan->name);
	network_send((t_com *)c, com, "C:", 2);
	network_send((t_com *)c, com, c->current_chan->name, len_chan);
	network_send((t_com *)c, com, ":", 1);
	network_send((t_com *)c, com, c->nick, ft_strlen(c->nick));
	network_send((t_com *)c, com, ":", 1);
	network_send((t_com *)c, com, cbuf, size);
}

size_t		send_msg(t_client *c, t_cbuff *cbuf, size_t size)
{
	t_irc			*server;
	t_connected		*cursor;
	t_com			*com;

	if (irc_check_nick_and_chan(c))
		return (size);
	server = (t_irc *)c->priv.port;
	cursor = server->connected;
	com = NULL;
	while (cursor)
	{
		if (cursor->channel == c->current_chan)
		{
			com = (t_com *)cursor->client;
			irc_send_msg(c, cbuf, size, com);
		}
		cursor = cursor->next;
	}
	if (com)
		return (0);
	return (size);
}
