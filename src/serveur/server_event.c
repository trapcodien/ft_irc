/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   server_event.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: garm <garm@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/28 01:23:44 by garm              #+#    #+#             */
/*   Updated: 2015/12/01 01:23:52 by garm             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "server.h"

static void		event_disconnect(t_client *c)
{
	t_irc		*server;
	t_connected	*cursor;
	t_connected	*prev;

	server = (t_irc *)c->priv.port;
	cursor = server->connected;
	while (cursor)
	{
		prev = cursor;
		cursor = cursor->next;
		if (prev->client == c)
			leave_channel(c, prev->channel);
	}
	if (server->verbose)
		ft_putendl("CLIENT LEFT.");
}

static size_t	event_recv(t_client *c, t_cbuff *cbuf, size_t size)
{
	char	dash;
	t_cmd	cmd;
	t_irc	*server;

	server = (t_irc *)c->priv.port;
	if (server->verbose)
		cbuff_write_noflush(1, cbuf, size);
	cbuff_consume_noflush(cbuf, &dash, sizeof(char));
	if (dash == '/')
	{
		cmd = extract_cmd(cbuf, size);
		execute_cmd(c, cmd);
		ft_strdel(&cmd.str1);
		ft_strdel(&cmd.str2);
		return (size);
	}
	return (send_msg(c, cbuf, size));
}

static void		event_ack(t_client *c, void *mem, size_t size)
{
	ft_memdel(&mem);
	(void)c;
	(void)size;
}

void			event_connect(t_client *c)
{
	t_com	*com;
	t_irc	*irc;

	irc = (t_irc *)c->priv.port;
	com = (t_com *)c;
	ack_hook(com, &event_ack);
	if (irc->verbose)
		ft_putendl("NEW CLIENT.");
	disconnect_hook(com, &event_disconnect);
	recv_hook(com, &event_recv);
	network_recv_mode(com, MODE_TEXT);
	irc_msg(c, "Welcome.");
}
