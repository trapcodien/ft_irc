/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   server_parser.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: garm <garm@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/04/04 00:17:16 by garm              #+#    #+#             */
/*   Updated: 2015/11/19 16:08:17 by garm             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "server.h"

static char		*sanitize_cmd(char *cmd)
{
	char	*trimmed;

	if (!cmd)
		return (NULL);
	trimmed = ft_strtrim(cmd);
	ft_strdel(&cmd);
	return (trimmed);
}

static void		extract_composed_cmd(char *str, t_cmd *cmd, size_t size)
{
	cmd->str1 = ft_strnew(size);
	if (ft_sscanf(str, "/nick %s", cmd->str1) == 1)
		cmd->type = CMD_NICK;
	else if (ft_sscanf(str, "/join %s", cmd->str1) == 1)
		cmd->type = CMD_JOIN;
	else if (ft_sscanf(str, "/leave %s", cmd->str1) == 1)
		cmd->type = CMD_LEAVE;
	else if (ft_strequ(str, "/help"))
		cmd->type = CMD_HELP;
	else
	{
		cmd->str2 = ft_strnew(size);
		if (ft_sscanf(str, "/msg %s %s", cmd->str1, cmd->str2) == 2)
			cmd->type = CMD_MSG;
		else
		{
			ft_strdel(&cmd->str1);
			ft_strdel(&cmd->str2);
		}
	}
}

t_cmd			extract_cmd(t_cbuff *cbuf, size_t size)
{
	t_cmd	cmd;
	char	*str;

	str = ft_strnew(size);
	cbuff_consume_noflush(cbuf, str, size);
	str = sanitize_cmd(str);
	ft_bzero(&cmd, sizeof(t_cmd));
	if (ft_sscanf(str, "/who") != ERROR)
		cmd.type = CMD_WHO;
	else if (ft_sscanf(str, "/leave") != ERROR)
		cmd.type = CMD_LEAVE;
	else if (ft_sscanf(str, "/exit") != ERROR)
		cmd.type = CMD_EXIT;
	else
		extract_composed_cmd(str, &cmd, size);
	ft_strdel(&str);
	cmd.str1 = sanitize_cmd(cmd.str1);
	cmd.str2 = sanitize_cmd(cmd.str2);
	return (cmd);
}
