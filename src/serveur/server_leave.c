/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   server_leave.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: garm <garm@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/04/04 03:57:25 by garm              #+#    #+#             */
/*   Updated: 2015/04/06 03:35:08 by garm             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "server.h"

static t_chan	*ft_get_channel(t_client *c, char *chan_name)
{
	t_chan		*channel;
	t_irc		*server;

	server = (t_irc *)c->priv.port;
	channel = server->channels;
	while (channel)
	{
		if (ft_strequ(channel->name, chan_name))
			return (channel);
		channel = channel->next;
	}
	return (NULL);
}

void			leave_channel(t_client *c, t_chan *channel)
{
	t_irc		*server;
	t_connected	*cursor;

	server = (t_irc *)c->priv.port;
	cursor = server->connected;
	while (cursor)
	{
		if (cursor->channel == channel && cursor->client != c)
			irc_leave_msg(cursor->client, channel, c);
		cursor = cursor->next;
	}
	cursor = server->connected;
	while (cursor)
	{
		if (cursor->client == c && cursor->channel == channel)
		{
			ft_stack_del(&server->connected, cursor, NULL);
			return ;
		}
		cursor = cursor->next;
	}
}

static void		choose_other_channel(t_client *c, t_chan *channel)
{
	t_irc		*server;
	t_connected	*cursor;

	server = (t_irc *)c->priv.port;
	cursor = server->connected;
	while (cursor)
	{
		if (cursor->client == c && cursor->channel != channel)
		{
			c->current_chan = cursor->channel;
			return ;
		}
		cursor = cursor->next;
	}
	c->current_chan = NULL;
}

void			cmd_leave(t_client *c, char *chan_name)
{
	t_chan	*channel;

	if (!c->current_chan)
		return (irc_msg(c, "not in a chan"));
	if (!chan_name || !*chan_name)
	{
		channel = c->current_chan;
		chan_name = channel->name;
	}
	else
		channel = ft_get_channel(c, chan_name);
	if (!channel)
		return (irc_msg(c, "chan not found"));
	irc_leave_confirmation(c, chan_name);
	if (channel == c->current_chan)
		choose_other_channel(c, channel);
	leave_channel(c, channel);
}

void			irc_leave_msg(t_client *c, t_chan *chan, t_client *who)
{
	char		*nick;

	nick = NULL;
	if (who->nick[0])
		nick = ft_strdup(who->nick);
	network_send(&c->priv, &c->priv, "S:", 2);
	network_send(&c->priv, &c->priv, chan->name, ft_strlen(chan->name));
	network_send(&c->priv, &c->priv, ":", 1);
	if (nick)
		network_sendack(&c->priv, &c->priv, nick, ft_strlen(nick));
	network_send(&c->priv, &c->priv, " leave", 6);
	network_send(&c->priv, &c->priv, "\n", 1);
}
