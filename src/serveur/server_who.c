/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   server_who.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: garm <garm@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/04/06 02:00:25 by garm              #+#    #+#             */
/*   Updated: 2015/04/06 02:06:30 by garm             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "server.h"

static void			who_msg(t_client *c, char *who)
{
	t_chan			*channel;

	channel = c->current_chan;
	network_send(&c->priv, &c->priv, "S:", 2);
	network_send(&c->priv, &c->priv, channel->name, ft_strlen(channel->name));
	network_send(&c->priv, &c->priv, ":who:", 5);
	network_send(&c->priv, &c->priv, who, ft_strlen(who));
	network_send(&c->priv, &c->priv, "\n", 1);
}

void				cmd_who(t_client *c)
{
	t_irc			*server;
	t_connected		*cursor;

	if (!c->current_chan)
		irc_msg(c, "not in a chan");
	else
	{
		server = (t_irc *)c->priv.port;
		cursor = server->connected;
		while (cursor)
		{
			if (cursor->channel == c->current_chan)
				who_msg(c, cursor->client->nick);
			cursor = cursor->next;
		}
	}
}
