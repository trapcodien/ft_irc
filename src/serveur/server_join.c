/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   server_join.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: garm <garm@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/04/04 04:01:47 by garm              #+#    #+#             */
/*   Updated: 2015/11/28 00:50:40 by garm             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "server.h"

static int			is_connected(t_client *c, t_chan *channel)
{
	t_irc			*server;
	t_connected		*cursor;

	server = (t_irc *)c->priv.port;
	cursor = server->connected;
	while (cursor)
	{
		if (cursor->client == c && cursor->channel == channel)
			return (1);
		cursor = cursor->next;
	}
	return (0);
}

static void			connect_client(t_irc *server, t_client *c, t_chan *chan)
{
	t_connected		*link;

	link = ft_memalloc(sizeof(t_connected));
	link->client = c;
	link->channel = chan;
	ft_stack_push(&server->connected, link);
}

void				cmd_join(t_client *c, char *chan_name)
{
	t_irc			*server;
	t_chan			*chan;
	size_t			len;
	t_bool			just_connected;

	just_connected = FALSE;
	len = 0;
	while (chan_name[len])
	{
		if (!ft_isalnum(chan_name[len]) && chan_name[len] != '_')
			return (irc_msg(c, "bad channel name"));
		len++;
	}
	server = (t_irc *)c->priv.port;
	chan = create_channel(server, chan_name);
	if (!is_connected(c, chan))
	{
		just_connected = TRUE;
		connect_client(server, c, chan);
	}
	c->current_chan = chan;
	irc_join_confirmation(c, chan_name);
	if (just_connected)
		irc_join_broadcast(c);
}

void				irc_join_msg(t_client *c, t_chan *chan, t_client *who)
{
	char			*nick;

	nick = NULL;
	if (who->nick[0])
		nick = ft_strdup(who->nick);
	network_send(&c->priv, &c->priv, "S:", 2);
	network_send(&c->priv, &c->priv, chan->name, ft_strlen(chan->name));
	network_send(&c->priv, &c->priv, ":", 1);
	if (nick)
		network_sendack(&c->priv, &c->priv, nick, ft_strlen(nick));
	network_send(&c->priv, &c->priv, " join", 5);
	network_send(&c->priv, &c->priv, "\n", 1);
}

void				irc_join_broadcast(t_client *c)
{
	t_chan			*chan;
	t_connected		*cursor;

	chan = c->current_chan;
	cursor = ((t_irc *)c->priv.port)->connected;
	while (cursor)
	{
		if (c->nick[0] && cursor->channel == chan && cursor->client != c)
			irc_join_msg(cursor->client, chan, c);
		cursor = cursor->next;
	}
}
