/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   server_channel.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: garm <garm@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/04/05 21:59:30 by garm              #+#    #+#             */
/*   Updated: 2015/04/06 00:57:38 by garm             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "server.h"

static void	ft_destroy_chan(t_chan *channel)
{
	if (channel->name)
		ft_strdel(&channel->name);
}

void		destroy_channel(t_irc *server, t_chan *channel)
{
	ft_stack_del(&server->channels, channel, &ft_destroy_chan);
}

void		destroy_all_channels(t_irc *server)
{
	while (server->channels)
		ft_stack_pop(&server->channels, &ft_destroy_chan);
}

t_chan		*create_channel(t_irc *server, char *chan_name)
{
	t_chan	*cursor;
	t_chan	*channel;

	channel = NULL;
	cursor = server->channels;
	while (cursor)
	{
		if (ft_strequ(cursor->name, chan_name))
			channel = cursor;
		cursor = cursor->next;
	}
	if (channel)
		return (channel);
	channel = ft_memalloc(sizeof(t_chan));
	if (!channel)
		return (NULL);
	channel->name = ft_strdup(chan_name);
	ft_stack_push(&server->channels, channel);
	return (channel);
}
