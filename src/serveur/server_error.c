/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   server_error.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: garm <garm@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/28 01:28:04 by garm              #+#    #+#             */
/*   Updated: 2015/11/28 01:29:04 by garm             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "server.h"

int			ft_usage(char *prog_name)
{
	ft_printf("Usage: %s [-v] <port>\n", prog_name);
	return (1);
}

int			check_error(t_net *net, t_port *server)
{
	if (!server || !server->tcps)
	{
		ft_putendl_fd("Error: unable to bind port.", 2);
		ft_memdel((void **)&net);
		return (1);
	}
	else if (server->tcps->error)
	{
		ft_memdel((void **)&net);
		return (ftsock_perror(server->tcps->error));
	}
	return (0);
}
