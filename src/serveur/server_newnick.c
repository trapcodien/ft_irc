/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   server_newnick.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: garm <garm@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/28 02:49:57 by garm              #+#    #+#             */
/*   Updated: 2015/11/28 02:55:46 by garm             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "server.h"

void		irc_msg_newnick(t_client *c, char *nickname)
{
	size_t	nick_len;

	nick_len = ft_strlen(nickname);
	network_send(&c->priv, &c->priv, "newNick:", 8);
	network_sendack(&c->priv, &c->priv, ft_strdup(nickname), nick_len);
	network_send(&c->priv, &c->priv, "\n", 1);
}
