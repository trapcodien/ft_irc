/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   server_execute.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: garm <garm@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/04/04 01:45:52 by garm              #+#    #+#             */
/*   Updated: 2015/12/01 05:22:35 by garm             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "server.h"

static void	irc_send(t_com *c, char *msg)
{
	network_send(c, c, "S:", 2);
	network_send(c, c, msg, ft_strlen(msg));
	network_send(c, c, "\n", 1);
}

static void	irc_help(t_com *c)
{
	irc_send(c, "------------------------------- HELP ------------------------\
-------");
	irc_send(c, "/help - Display this help.");
	irc_send(c, "/connect <host> <port> - connect to another chat server.");
	irc_send(c, "/join <channel name> - join a channel, create it if does \
not exist.");
	irc_send(c, "/nick <nickname> - Change nickname.");
	irc_send(c, "/msg <nickname> <msg> - Send a private message to an user.");
	irc_send(c, "/leave - Leave current channel.");
	irc_send(c, "/leave <channel name> - Leave specified channel.");
	irc_send(c, "/who - display channel users.");
	irc_send(c, "/exit - Disconnect and quit client.");
}

void		execute_cmd(t_client *c, t_cmd cmd)
{
	if (cmd.type == CMD_NICK && cmd.str1)
		cmd_nick(c, cmd.str1);
	else if (cmd.type == CMD_JOIN && cmd.str1 && ft_strequ("", c->nick))
		irc_msg(c, "You must have a nickname for join a channel");
	else if (cmd.type == CMD_JOIN && cmd.str1)
		cmd_join(c, cmd.str1);
	else if (cmd.type == CMD_LEAVE)
		cmd_leave(c, cmd.str1);
	else if (cmd.type == CMD_WHO)
		cmd_who(c);
	else if (cmd.type == CMD_MSG && cmd.str1 && cmd.str2)
		cmd_msg(c, cmd.str1, cmd.str2);
	else if (cmd.type == CMD_EXIT)
		network_close(&c->priv);
	else if (cmd.type == CMD_HELP)
		irc_help(&c->priv);
	else
		irc_msg(c, "bad cmd");
}
