/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   server_stdin.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: garm <garm@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/01 06:23:18 by garm              #+#    #+#             */
/*   Updated: 2015/12/01 06:46:03 by garm             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "server.h"

static void		server_say(t_com *c, char *buffer, size_t size)
{
	while (c)
	{
		network_send(c, c, "msgServer:", 10);
		network_send(c, c, buffer + 4, size - 4);
		c = c->next;
	}
}

static void		server_list(t_com *c)
{
	if (c)
		ft_putendl("---------------- USERS ----------------");
	while (c)
	{
		if (((t_client *)c)->nick[0] == '\0')
			ft_printf("Anonymous (%s:%i)\n", c->tcps->ip, \
									c->tcps->port);
		else
			ft_printf("%s (%s:%i)\n", (((t_client *)c)->nick), \
									c->tcps->ip, c->tcps->port);
		c = c->next;
	}
}

static void		server_kick(t_com *c, char *buffer, size_t size)
{
	t_client	*client;

	while (c)
	{
		client = (t_client *)c;
		buffer[size - 1] = '\0';
		if (!ft_strcmp(client->nick, buffer + 5))
		{
			if (*(client->nick))
			{
				ft_putendl("User kicked.");
				network_close(c);
			}
		}
		if (!ft_strcmp(buffer + 5, "Anonymous") && !*(client->nick))
		{
			ft_putendl("Anonymous kicked.");
			network_close(c);
		}
		c = c->next;
	}
}

static void		server_help(void)
{
	ft_putendl("---------------- HELP ----------------");
	ft_putendl("help\t\t\tDisplay this help.");
	ft_putendl("exit\t\t\tClose server.");
	ft_putendl("kick <nickname>\t\tKick someone, \
if 'Anonymous' is used, kick all Anonymous.");
	ft_putendl("say\t\t\tTalk to all connected clients.");
	ft_putendl("list\t\t\tList all connected clients.");
}

void			event_stdin(t_net *net, char *buffer, size_t size)
{
	t_com		*cursor;

	if (!ft_strcmp(buffer, "exit\n"))
		return (network_exit(net));
	cursor = net->servers->clients;
	if (!ft_strncmp("say ", buffer, 4))
		server_say(cursor, buffer, size);
	else if (!ft_strcmp("list\n", buffer))
		server_list(cursor);
	else if (!ft_strncmp("kick ", buffer, 5))
		server_kick(cursor, buffer, size);
	else if (!ft_strcmp("help\n", buffer))
		server_help();
}
