/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   libftdata.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: garm <garm@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/06/18 23:52:49 by garm              #+#    #+#             */
/*   Updated: 2015/11/27 21:18:29 by garm             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LIBFTDATA_H
# define LIBFTDATA_H

# include "libft.h"

# define S_P1 "L0KiPEwowXmGbW1Q8v-sNg1e6GaAcgh4lF9-t5IkqBpr5WYc7AQ7jHOyn3bCUdL4"
# define S_P2 "BhMaCEDJu2Vfs_6SdjZkrTexJY8R0PqOTnHtSyF3Nfi9pVMzIUxolZRzuX2_mKvD"
# define HTABLE_SALT S_P1 S_P2
# define HTABLE_SIZE (256 * 256)

# define DEFAULT_RESIZE 32

# define CBUF_EMPTY 0
# define CBUF_NORMAL 1
# define CBUF_FULL 2

# define CBUF_GET_DISTANCE(s, d, size) ((d >= s) ? (d - s) : (size + (d - s)))

typedef void		*(*t_data_construct)(void);
typedef void		(*t_data_destruct)(void *);
typedef void		(*t_data_display)(void *);

typedef struct		s_stack
{
	struct s_stack	*next;
}					t_stack;

typedef struct		s_queue
{
	struct s_queue	*next;
	struct s_queue	*tail;
}					t_queue;

typedef struct		s_elsd
{
	struct s_elsd	*next;
	struct s_elsd	*prev;
}					t_elsd;

typedef struct		s_lsd
{
	void			*head;
	void			*tail;
}					t_lsd;

typedef struct		s_ht
{
	char			*id;
	void			*data;
	struct s_ht		*left;
	struct s_ht		*right;
}					t_ht;

typedef struct		s_dbuf
{
	void			*data;
	size_t			resize;
	size_t			size;
	long			offset;
}					t_dbuf;

typedef struct		s_cbuf
{
	void			*data;
	size_t			size;
	off_t			off_c;
	off_t			off_p;
	char			state;
}					t_cbuf;

typedef struct		s_btree
{
	struct s_btree	*left;
	struct s_btree	*right;
}					t_btree;

typedef struct		s_point
{
	int				x;
	int				y;
}					t_point;

typedef struct		s_vector
{
	t_point			a;
	t_point			b;
	int				x;
	int				y;
}					t_vector;

/*
** Stacks
*/
void				*ft_stack_create(void *f_construct);
void				ft_stack_destroy(void *a_elem, void *f_destruct);
void				ft_stack_push(void *a_stack, void *new_elem);
void				ft_stack_pop(void *a_stack, void *f_destruct);
void				ft_stack_debug(void *stack, void *f_display);
void				ft_stack_del(void *a_stack, void *todel, void *f_destruct);

/*
** Queues
*/
void				*ft_queue_create(void *f_construct);
void				ft_queue_destroy(void *a_elem, void *f_destruct);
void				ft_queue_push(void *a_queue, void *new_elem);
void				ft_queue_pop(void *a_queue, void *f_destruct);
void				ft_queue_debug(void *queue, void *f_display);
void				ft_queue_insert(void *a_queue, void *prev, void *new_elem);

/*
** LSD is not a drug, just double linked lists implementation
*/
t_lsd				*ft_lsd_create(void);
void				ft_lsd_destroy(t_lsd **lsd);
void				ft_lsd_insert_left(t_lsd *lsd, void *new_elsd, void *elsd);
void				ft_lsd_insert_right(t_lsd *lsd, void *elsd, void *new_elsd);
void				*ft_lsd_pick(t_lsd *lsd, void *a_elsd);
void				ft_lsd_pushfront(t_lsd *lsd, void *new_elsd);
void				ft_lsd_pushback(t_lsd *lsd, void *new_elsd);
void				*ft_lsd_popfront(t_lsd *lsd);
void				*ft_lsd_popback(t_lsd *lsd);

/*
** Hash Tables (collisions managed with binary trees)
*/
t_ht				**ft_htable_create(void);
void				ft_htable_destroy(t_ht **hashtable);
int					ft_htable_hash(char *id);
void				*ft_htable_getdata(t_ht **hashtable, char *id);
void				ft_htable_setdata(t_ht **hashtable, char *id, void *data);

/*
** Dynamic Buffers
*/
t_dbuf				*ft_dbuf_create(size_t resize);
void				ft_dbuf_destroy(t_dbuf **b);
int					ft_dbuf_write(t_dbuf *b, void *data, size_t len);
int					ft_dbuf_write_str(t_dbuf *b, const char *str);
int					ft_dbuf_set(t_dbuf *b, char c, long shift, size_t len);
void				ft_dbuf_bzero(t_dbuf *b);
t_dbuf				*ft_dbuf_sub(t_dbuf *b, long shift, size_t len);
t_dbuf				*ft_dbuf_dup(t_dbuf *b);

/*
** Circular Buffer
*/
t_cbuf				*ft_cbuf_create(size_t size);
void				ft_cbuf_destroy(t_cbuf **b);
ssize_t				ft_cbuf_read(int fd, t_cbuf *cbuf, size_t nbytes);
ssize_t				ft_cbuf_produce(t_cbuf *cbuf, void *data, size_t size);
ssize_t				ft_cbuf_write(int fd, t_cbuf *cbuf, size_t nbytes);
ssize_t				ft_cbuf_write_noflush(int fd, t_cbuf *cbuf, size_t nbytes);
ssize_t				ft_cbuf_consume(t_cbuf *cbuf, void *data, size_t size);
ssize_t				ft_cbuf_consume_noflush(t_cbuf *b, void *d, size_t size);
size_t				ft_cbuf_get_distance(t_cbuf *cbuf, size_t src, size_t dst);
ssize_t				ft_cbuf_findchr(t_cbuf *cbuf, char c, off_t o, size_t len);
ssize_t				ft_cbuf_findrchr(t_cbuf *cbuf, char c, off_t o, size_t len);
int					ft_cbuf_debug(t_cbuf *cbuf);

/*
** Binary Search Tree
*/
void				*bst_insert(void *tree, void *node, int (*cmp)());
void				bst_foreach(void *tree, void (*func)());
void				bst_eforeach(void *tree, void *env, void (*f)());
void				bst_destroy(void *tree);

/*
** Point
*/
t_point				ft_point(int x, int y);

/*
** Vector
*/
t_vector			ft_vector(t_point a, t_point b);
int					ft_vector_cmpsize(t_vector v1, t_vector v2);

#endif
