/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_bst_destroy.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: garm <garm@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/03/23 21:28:48 by garm              #+#    #+#             */
/*   Updated: 2015/04/01 16:09:01 by garm             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libftdata.h"

void	bst_destroy(void *t)
{
	t_btree		*tree;

	tree = t;
	if (tree->left)
		bst_destroy(tree->left);
	if (tree->right)
		bst_destroy(tree->right);
	ft_memdel(&t);
}
