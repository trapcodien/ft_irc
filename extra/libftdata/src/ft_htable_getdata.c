/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_htable_getdata.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: garm <garm@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/02/19 15:14:43 by garm              #+#    #+#             */
/*   Updated: 2014/10/28 14:11:56 by garm             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libftdata.h"

static t_ht		*bt_search(t_ht *root, char *id)
{
	if (root && *id)
	{
		if (ft_strcmp(root->id, id) > 0)
			return (bt_search(root->left, id));
		if (ft_strcmp(root->id, id) < 0)
			return (bt_search(root->right, id));
		if (ft_strcmp(root->id, id) == 0)
			return (root);
	}
	return (NULL);
}

void			*ft_htable_getdata(t_ht **hashtable, char *id)
{
	int			hash;
	t_ht		*search;

	hash = ft_htable_hash(id);
	if (hashtable[hash])
	{
		search = bt_search(hashtable[hash], id);
		if (search)
			return (search->data);
		else
			return (NULL);
	}
	else
		return (NULL);
}
