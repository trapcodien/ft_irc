/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_cbuf_debug.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: garm <garm@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/10/28 10:41:30 by garm              #+#    #+#             */
/*   Updated: 2014/10/28 14:11:03 by garm             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libftdata.h"

static void		display_normal_data(int left, int middle, int right)
{
	int			i;

	i = 0;
	while (i < left)
	{
		ft_putchar('.');
		i++;
	}
	i = 0;
	while (i < middle)
	{
		ft_putchar('*');
		i++;
	}
	i = 0;
	while (i < right)
	{
		ft_putchar('.');
		i++;
	}
}

static void		display_overlap_data(int left, int middle, int right)
{
	int			i;

	i = 0;
	while (i < left)
	{
		ft_putchar('*');
		i++;
	}
	i = 0;
	while (i < middle)
	{
		ft_putchar('.');
		i++;
	}
	i = 0;
	while (i < right)
	{
		ft_putchar('*');
		i++;
	}
}

static int		u_set_normal(t_cbuf *cbuf, int *left, int *middle, int *right)
{
	off_t		*p;
	off_t		*c;

	p = &(cbuf->off_p);
	c = &(cbuf->off_c);
	*middle = ((double)100 / ((double)cbuf->size / (*p - *c)));
	*right = ((double)100 / ((double)cbuf->size / (cbuf->size - *p)));
	*left = 100 - *middle - *right;
	return (0);
}

static int		u_set_overlap(t_cbuf *cbuf, int *left, int *middle, int *right)
{
	off_t		*p;
	off_t		*c;

	p = &(cbuf->off_p);
	c = &(cbuf->off_c);
	*middle = ((double)100 / ((double)cbuf->size / (*c - *p)));
	*right = ((double)100 / ((double)cbuf->size / (cbuf->size - *c)));
	*left = 100 - *middle - *right;
	display_overlap_data(*left, *middle, *right);
	ft_putstr("]\n");
	return (0);
}

int				ft_cbuf_debug(t_cbuf *cbuf)
{
	int		left;
	int		middle;
	int		right;
	off_t	*p;
	off_t	*c;

	ft_putchar('[');
	left = 0;
	right = 0;
	middle = 0;
	if (cbuf)
	{
		p = &(cbuf->off_p);
		c = &(cbuf->off_c);
		if (cbuf->state == CBUF_FULL)
			middle = 100;
		else if (cbuf->state == CBUF_EMPTY)
			left = 100;
		else if (cbuf->state == CBUF_NORMAL && *c < *p)
			u_set_normal(cbuf, &left, &middle, &right);
		else if (cbuf->state == CBUF_NORMAL && *p < *c)
			return (u_set_overlap(cbuf, &left, &middle, &right));
		display_normal_data(left, middle, right);
	}
	return (ft_putstr("]\n"));
}
