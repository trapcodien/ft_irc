/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_dbuf_create.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: garm <garm@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/06/20 20:06:55 by garm              #+#    #+#             */
/*   Updated: 2014/06/20 20:16:35 by garm             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libftdata.h"

t_dbuf		*ft_dbuf_create(size_t resize)
{
	void	*data;
	t_dbuf	*buffer;

	if (resize == 0)
		resize = DEFAULT_RESIZE;
	buffer = (t_dbuf *)ft_memalloc(sizeof(t_dbuf));
	if (!buffer)
		return (NULL);
	data = ft_memalloc(resize);
	if (!data)
		return (NULL);
	buffer->data = data;
	buffer->resize = resize;
	buffer->size = resize;
	buffer->offset = 0;
	return (buffer);
}
