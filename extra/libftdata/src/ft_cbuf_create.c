/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_cbuf_create.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: garm <garm@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/06/29 22:26:57 by garm              #+#    #+#             */
/*   Updated: 2014/10/28 00:30:42 by garm             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libftdata.h"

t_cbuf		*ft_cbuf_create(size_t size)
{
	t_cbuf		*new;

	if (!size)
		return (NULL);
	new = ft_memalloc(sizeof(t_cbuf));
	if (!new)
		return (NULL);
	new->data = ft_memalloc(size);
	if (!new->data)
	{
		ft_memdel((void **)&new);
		return (NULL);
	}
	new->size = size;
	new->off_c = 0;
	new->off_p = 0;
	new->state = CBUF_EMPTY;
	return (new);
}
