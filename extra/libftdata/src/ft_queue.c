/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_queue.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: garm <garm@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/26 10:27:57 by garm              #+#    #+#             */
/*   Updated: 2014/12/15 00:30:11 by garm             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libftdata.h"

void		*ft_queue_create(void *f_construct)
{
	t_data_construct	c;
	void				*queue;

	if ((c = f_construct))
		queue = c();
	else
		queue = ft_memalloc(sizeof(t_queue));
	if (!queue)
		return (NULL);
	((t_queue *)queue)->next = NULL;
	((t_queue *)queue)->tail = NULL;
	return (queue);
}

void		ft_queue_destroy(void *a_elem, void *f_destruct)
{
	t_data_destruct		d;
	t_queue				**elem;

	elem = (t_queue **)a_elem;
	if (!elem || !*elem)
		return ;
	if ((d = f_destruct))
		d(*elem);
	ft_memdel(a_elem);
}

void		ft_queue_push(void *a_queue, void *new_elem)
{
	t_queue		**queue;
	t_queue		*new;

	if (!a_queue || !new_elem)
		return ;
	queue = (t_queue **)a_queue;
	new = new_elem;
	if (!*queue)
		*queue = new;
	if ((*queue)->tail)
		(*queue)->tail->next = new;
	(*queue)->tail = new;
}

void		ft_queue_pop(void *a_queue, void *f_destruct)
{
	t_queue		**queue;
	t_queue		*todel;

	queue = (t_queue **)a_queue;
	if (!queue || !*queue)
		return ;
	todel = *queue;
	*queue = (*queue)->next;
	if (*queue)
		(*queue)->tail = todel->tail;
	ft_queue_destroy(&todel, f_destruct);
}

void		ft_queue_insert(void *a_queue, void *prev_elem, void *new_elem)
{
	t_queue		**queue;
	t_queue		*new;
	t_queue		*prev;

	if (!a_queue || !new_elem)
		return ;
	queue = (t_queue **)a_queue;
	new = new_elem;
	prev = prev_elem;
	if (!*queue)
		*queue = new;
	if (!prev)
		return ;
	new->next = prev->next;
	if (!new->next)
		(*queue)->tail = new;
	prev->next = new;
}
