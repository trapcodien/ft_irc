/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_dbuf_dup.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: garm <garm@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/06/24 02:08:28 by garm              #+#    #+#             */
/*   Updated: 2014/06/24 02:12:36 by garm             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libftdata.h"

t_dbuf		*ft_dbuf_dup(t_dbuf *b)
{
	t_dbuf		*new_buf;
	long		old_offset;

	if (!b)
		return (NULL);
	old_offset = b->offset;
	b->offset = 0;
	new_buf = ft_dbuf_sub(b, 0, b->size);
	b->offset = old_offset;
	if (new_buf)
		new_buf->offset = old_offset;
	return (new_buf);
}
