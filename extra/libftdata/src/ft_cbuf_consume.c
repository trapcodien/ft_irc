/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_cbuf_consume.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: garm <garm@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/10/28 03:18:24 by garm              #+#    #+#             */
/*   Updated: 2014/10/28 13:32:55 by garm             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libftdata.h"

static void		*utils_ft_memcpy(void *s1, const void *s2, size_t n)
{
	size_t			i;
	char			*str1;
	const char		*str2;

	if (!s1 || !s2)
		return (s1);
	str1 = (char *)s1;
	str2 = (const char *)s2;
	i = 0;
	while (i < n)
	{
		str1[i] = str2[i];
		i++;
	}
	return (s1);
}

ssize_t			ft_cbuf_consume(t_cbuf *cbuf, void *data, size_t size)
{
	size_t		rbyte;
	off_t		*p;
	off_t		*c;

	if (!cbuf)
		return (ERROR);
	if (!size || cbuf->state == CBUF_EMPTY)
		return (0);
	p = &(cbuf->off_p);
	c = &(cbuf->off_c);
	rbyte = (*p > *c) ? (*p - *c) : ((off_t)cbuf->size - *c);
	rbyte = (size < rbyte) ? (size) : (rbyte);
	if (!rbyte)
		return (0);
	utils_ft_memcpy(data, ((char *)cbuf->data) + *c, rbyte);
	*c += rbyte;
	if (*c == (off_t)cbuf->size)
		*c = 0;
	cbuf->state = (*c == *p) ? (CBUF_EMPTY) : (CBUF_NORMAL);
	return (rbyte + ft_cbuf_consume(cbuf, (char *)data + rbyte, size - rbyte));
}

ssize_t			ft_cbuf_consume_noflush(t_cbuf *cbuf, void *data, size_t size)
{
	off_t		old_consummer_offset;
	char		old_state;
	ssize_t		ret;

	if (!cbuf)
		return (ERROR);
	old_consummer_offset = cbuf->off_c;
	old_state = cbuf->state;
	ret = ft_cbuf_consume(cbuf, data, size);
	cbuf->state = old_state;
	cbuf->off_c = old_consummer_offset;
	return (ret);
}
