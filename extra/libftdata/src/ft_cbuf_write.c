/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_cbuf_write.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: garm <garm@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/06/29 22:33:39 by garm              #+#    #+#             */
/*   Updated: 2014/11/26 01:03:20 by garm             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libftdata.h"

ssize_t			ft_cbuf_write(int fd, t_cbuf *cbuf, size_t nbytes)
{
	ssize_t		ret;
	size_t		rbytes;
	off_t		*p;
	off_t		*c;

	if (!cbuf)
		return (ERROR);
	if (!nbytes || cbuf->state == CBUF_EMPTY)
		return (0);
	p = &(cbuf->off_p);
	c = &(cbuf->off_c);
	rbytes = (*p > *c) ? (*p - *c) : ((off_t)cbuf->size - *c);
	rbytes = (nbytes < rbytes) ? (nbytes) : (rbytes);
	ret = write(fd, ((char *)cbuf->data) + *c, rbytes);
	if (ret <= 0 && rbytes)
		return (ERROR);
	*c += ret;
	if (*c == (off_t)cbuf->size)
		*c = 0;
	cbuf->state = (*c == *p) ? (CBUF_EMPTY) : (CBUF_NORMAL);
	return (ret + ft_cbuf_write(fd, cbuf, nbytes - ret));
}

ssize_t			ft_cbuf_write_noflush(int fd, t_cbuf *cbuf, size_t nbytes)
{
	off_t		old_consummer_offset;
	char		old_state;
	ssize_t		ret;

	if (!cbuf)
		return (ERROR);
	old_consummer_offset = cbuf->off_c;
	old_state = cbuf->state;
	ret = ft_cbuf_write(fd, cbuf, nbytes);
	cbuf->state = old_state;
	cbuf->off_c = old_consummer_offset;
	return (ret);
}
