/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lsd_pick.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: garm <garm@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/28 02:52:33 by garm              #+#    #+#             */
/*   Updated: 2014/11/28 08:53:52 by garm             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libftdata.h"

static void	*ft_lsd_del(t_elsd **elem, t_elsd *new_ptr_elem)
{
	t_elsd	*tmp;

	tmp = *elem;
	*elem = new_ptr_elem;
	if (tmp->next)
		tmp->next->prev = tmp->prev;
	if (tmp->prev)
		tmp->prev->next = tmp->next;
	tmp->next = NULL;
	tmp->prev = NULL;
	return (tmp);
}

void		*ft_lsd_pick(t_lsd *lsd, void *a_elsd)
{
	t_elsd	**elem;
	t_elsd	*new_ptr_elem;
	t_elsd	*tmp;

	elem = (t_elsd **)a_elsd;
	if (!lsd || !lsd->head || !lsd->tail || !elem || !*elem)
		return (NULL);
	if (lsd->head == lsd->tail)
	{
		lsd->head = NULL;
		lsd->tail = NULL;
		tmp = *elem;
		*elem = NULL;
		return (tmp);
	}
	if (*elem == lsd->head)
		lsd->head = ((t_elsd *)lsd->head)->next;
	if (*elem == lsd->tail)
	{
		lsd->tail = ((t_elsd *)lsd->tail)->prev;
		new_ptr_elem = lsd->tail;
	}
	else
		new_ptr_elem = (*elem)->next;
	return (ft_lsd_del(elem, new_ptr_elem));
}
