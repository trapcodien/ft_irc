/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_lsd_insert.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: garm <garm@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/28 01:14:29 by garm              #+#    #+#             */
/*   Updated: 2014/11/28 20:51:37 by garm             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libftdata.h"

void		ft_lsd_insert_left(t_lsd *lsd, void *new_elsd, void *elsd)
{
	t_elsd	*elem;
	t_elsd	*new_elem;

	elem = elsd;
	new_elem = new_elsd;
	if (!lsd || !new_elem)
		return ;
	if (!lsd->head || !lsd->tail)
	{
		lsd->head = new_elem;
		lsd->tail = new_elem;
		return ;
	}
	if (!elem)
		return ;
	if (elem == lsd->head)
		lsd->head = new_elem;
	new_elem->next = elem;
	new_elem->prev = elem->prev;
	elem->prev = new_elem;
	if (new_elem->prev)
		new_elem->prev->next = new_elem;
}

void		ft_lsd_insert_right(t_lsd *lsd, void *elsd, void *new_elsd)
{
	t_elsd	*elem;
	t_elsd	*new_elem;

	elem = elsd;
	new_elem = new_elsd;
	if (!lsd || !new_elem)
		return ;
	if (!lsd->head || !lsd->tail)
	{
		lsd->head = new_elem;
		lsd->tail = new_elem;
		return ;
	}
	if (!elem)
		return ;
	if (elem == lsd->tail)
		lsd->tail = new_elem;
	new_elem->prev = elem;
	new_elem->next = elem->next;
	elem->next = new_elem;
	if (new_elem->next)
		new_elem->next->prev = new_elem;
}
