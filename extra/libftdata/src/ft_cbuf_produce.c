/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_cbuf_produce.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: garm <garm@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/10/28 03:11:08 by garm              #+#    #+#             */
/*   Updated: 2014/10/28 13:32:51 by garm             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libftdata.h"

static void		*utils_ft_memcpy(void *s1, const void *s2, size_t n)
{
	size_t			i;
	char			*str1;
	const char		*str2;

	if (!s1 || !s2)
		return (s1);
	str1 = (char *)s1;
	str2 = (const char *)s2;
	i = 0;
	while (i < n)
	{
		str1[i] = str2[i];
		i++;
	}
	return (s1);
}

ssize_t			ft_cbuf_produce(t_cbuf *cbuf, void *data, size_t size)
{
	size_t		rbyte;
	off_t		*p;
	off_t		*c;

	if (!cbuf)
		return (ERROR);
	if (!size || cbuf->state == CBUF_FULL)
		return (0);
	p = &(cbuf->off_p);
	c = &(cbuf->off_c);
	rbyte = (*c > *p) ? (*c - *p) : ((off_t)cbuf->size - *p);
	rbyte = (size < rbyte) ? (size) : (rbyte);
	if (!rbyte)
		return (0);
	utils_ft_memcpy(((char *)cbuf->data) + *p, data, rbyte);
	*p += rbyte;
	if (*p == (off_t)cbuf->size)
		*p = 0;
	cbuf->state = (*p == *c) ? (CBUF_FULL) : (CBUF_NORMAL);
	return (rbyte + ft_cbuf_produce(cbuf, (char *)data + rbyte, size - rbyte));
}
