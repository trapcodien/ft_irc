/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_dbuf_write_str.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: garm <garm@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/06/20 21:51:36 by garm              #+#    #+#             */
/*   Updated: 2014/10/28 14:11:27 by garm             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libftdata.h"

int		ft_dbuf_write_str(t_dbuf *buffer, const char *str)
{
	int		ret;

	ret = ft_dbuf_write(buffer, (void *)str, ft_strlen(str) + 1);
	if (ret == ERROR)
		return (ERROR);
	buffer->offset--;
	return (ret);
}
