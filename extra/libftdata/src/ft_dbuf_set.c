/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_dbuf_set.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: garm <garm@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/06/23 23:13:36 by garm              #+#    #+#             */
/*   Updated: 2014/10/28 14:11:12 by garm             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "libftdata.h"

int					ft_dbuf_set(t_dbuf *b, char c, long shift, size_t len)
{
	long		old_offset;
	void		*tmp_data;
	int			ret_write;

	if (!b || len == 0)
		return (-1);
	old_offset = b->offset;
	b->offset += shift;
	tmp_data = malloc(len);
	if (!tmp_data)
		return (-1);
	ft_memset(tmp_data, c, len);
	ret_write = ft_dbuf_write(b, tmp_data, len);
	ft_memdel(&tmp_data);
	b->offset = old_offset;
	return (ret_write);
}
