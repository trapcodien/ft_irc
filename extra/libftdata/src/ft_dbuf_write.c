/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_dbuf_write.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: garm <garm@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/06/20 20:32:35 by garm              #+#    #+#             */
/*   Updated: 2014/06/22 02:56:06 by garm             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libftdata.h"

static void		expand_data(t_dbuf *b, size_t added_bytes)
{
	size_t		new_size;
	void		*new_data;
	void		*data;

	if (added_bytes == 0 || !b || !(b->data))
		return ;
	data = b->data;
	new_size = added_bytes + b->size;
	new_data = ft_memalloc(new_size + 1);
	if (!new_data)
		return ;
	ft_memcpy(new_data, data, b->size);
	b->data = new_data;
	ft_memdel(&data);
	b->size += added_bytes;
}

static void		expandleft_data(t_dbuf *b, size_t added_bytes)
{
	size_t		new_size;
	void		*new_data;
	void		*data;

	if (added_bytes == 0 || !b || !(b->data))
		return ;
	data = b->data;
	new_size = added_bytes + b->size;
	new_data = ft_memalloc(new_size + 1);
	if (!new_data)
		return ;
	b->data = new_data;
	new_data = (char *)new_data + added_bytes;
	ft_memcpy(new_data, data, b->size);
	ft_memdel(&data);
	b->size += added_bytes;
	b->offset = 0;
}

int				ft_dbuf_write(t_dbuf *buffer, void *data, size_t len)
{
	size_t		finalsize;
	size_t		added_bytes;

	if (!buffer || !data)
		return (ERROR);
	if (len == 0)
		return (0);
	if (buffer->offset < 0)
		expandleft_data(buffer, ((long)(buffer->offset) * -1));
	finalsize = len + buffer->offset;
	added_bytes = finalsize - buffer->size;
	if (finalsize > buffer->size && added_bytes >= buffer->resize)
		expand_data(buffer, added_bytes);
	else if (finalsize > buffer->size && added_bytes < buffer->resize)
		expand_data(buffer, buffer->resize);
	ft_memcpy((char *)(buffer->data) + buffer->offset, data, len);
	buffer->offset += len;
	return (len);
}
