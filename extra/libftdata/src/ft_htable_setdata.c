/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_htable_setdata.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: garm <garm@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/02/19 15:01:58 by garm              #+#    #+#             */
/*   Updated: 2014/06/23 00:35:41 by garm             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libftdata.h"

static t_ht		*bt_new(char *id, void *data)
{
	t_ht		*node;

	node = (t_ht *)ft_memalloc(sizeof(t_ht));
	node->right = NULL;
	node->left = NULL;
	node->id = ft_strdup(id);
	node->data = data;
	return (node);
}

static void		bt_add(t_ht **root, char *id, void *data)
{
	if (!*root)
		*root = bt_new(id, data);
	else if (ft_strcmp((*root)->id, id) >= 0)
		bt_add(&(*root)->left, id, data);
	else
		bt_add(&(*root)->right, id, data);
}

static t_ht		*bt_search(t_ht *root, char *id)
{
	if (root && *id)
	{
		if (ft_strcmp(root->id, id) > 0)
			return (bt_search(root->left, id));
		if (ft_strcmp(root->id, id) < 0)
			return (bt_search(root->right, id));
		if (ft_strcmp(root->id, id) == 0)
			return (root);
	}
	return (NULL);
}

void			ft_htable_setdata(t_ht **hashtable, char *id, void *data)
{
	int			hash;
	t_ht		*search;

	hash = ft_htable_hash(id);
	if (hashtable[hash])
	{
		search = bt_search(hashtable[hash], id);
		if (search)
			search->data = data;
		else
			bt_add(&hashtable[hash], id, data);
	}
	else
		bt_add(&hashtable[hash], id, data);
}
