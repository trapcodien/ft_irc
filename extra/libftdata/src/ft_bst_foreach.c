/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_bst_foreach.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: garm <garm@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/03/23 22:21:00 by garm              #+#    #+#             */
/*   Updated: 2015/04/01 16:09:12 by garm             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libftdata.h"

void		bst_foreach(void *t, void (*func)())
{
	t_btree		*tree;

	tree = t;
	if (tree->left)
		bst_foreach(tree->left, func);
	func(tree);
	if (tree->right)
		bst_foreach(tree->right, func);
}

void		bst_eforeach(void *t, void *env, void (*f)())
{
	t_btree		*tree;

	tree = t;
	if (tree->left)
		bst_eforeach(tree->left, env, f);
	f(tree, env);
	if (tree->right)
		bst_eforeach(tree->right, env, f);
}
