/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_htable_destroy.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: garm <garm@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/02/19 08:13:17 by garm              #+#    #+#             */
/*   Updated: 2014/06/23 00:35:21 by garm             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libftdata.h"

static void		bt_destroy(t_ht *root)
{
	if (root)
	{
		if (root->left)
			bt_destroy(root->left);
		if (root->right)
			bt_destroy(root->right);
		ft_memdel((void **)&(root->id));
		ft_memdel((void **)&root);
	}
}

void			ft_htable_destroy(t_ht **hashtable)
{
	int		i;

	i = 0;
	while (i < HTABLE_SIZE)
	{
		if (hashtable[i])
			bt_destroy(hashtable[i]);
		i++;
	}
	ft_memdel((void **)&hashtable);
}
