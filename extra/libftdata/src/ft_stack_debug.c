/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_stack_debug.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: garm <garm@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/26 02:00:34 by garm              #+#    #+#             */
/*   Updated: 2014/11/26 02:04:10 by garm             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libftdata.h"

void		ft_stack_debug(void *stack, void *f_display)
{
	t_stack			*cursor;
	t_data_display	display_function;

	if (!stack || !f_display)
		return ;
	display_function = f_display;
	cursor = stack;
	while (cursor)
	{
		display_function(cursor);
		cursor = cursor->next;
	}
}
