# LIBFTDATA #

libftdata is a library which implement some data structures.
for use : you must have [libft](https://bitbucket.org/trapcodien/libftv2)
library.

Summary
=======
[Instructions](#markdown-header--instructions)
==================
* [Download](#markdown-header-download)
* [Compilation](#markdown-header-compilation)
* [First Use](#markdown-header-first-use)

[Stacks](#markdown-header--stacks)
==================
* [Presentation](#markdown-header-stacks-presentation)
* [Structure](#markdown-header-stacks-structure)
* [ft_stack_create](#markdown-header-ft_stack_create)
* [ft_stack_destroy](#markdown-header-ft_stack_destroy)
* [ft_stack_push](#markdown-header-ft_stack_push)
* [ft_stack_del](#markdown-header-ft_stack_del)
* [ft_stack_pop](#markdown-header-ft_stack_pop)
* [ft_stack_debug](#markdown-header-ft_stack_debug)

[Queues](#markdown-header--queues)
==================
* [Presentation](#markdown-header-queues-presentation)
* [Structure](#markdown-header-queues-structure)
* [ft_queue_create](#markdown-header-ft_queue_create)
* [ft_queue_create](#markdown-header-ft_queue_destroy)
* [ft_queue_push](#markdown-header-ft_queue_push)
* [ft_queue_pop](#markdown-header-ft_queue_pop)

[Doubly Linked Lists](#markdown-header--doubly-linked-lists)
==================
* [Presentation](#markdown-header-doubly-linked-lists-presentation)
* [Structure](#markdown-header-doubly-linked-lists-structures)
* [Schema](#markdown-header-doubly-linked-lists-schema)
* [ft_lsd_create](#markdown-header-ft_lsd_create)
* [ft_lsd_destroy](#markdown-header-ft_lsd_destroy)
* [ft_lsd_insert_left](#markdown-header-ft_lsd_insert_left)
* [ft_lsd_insert_right](#markdown-header-ft_lsd_insert_right)
* [ft_lsd_pick](#markdown-header-ft_lsd_pick)
* [ft_lsd_pushfront](#markdown-header-ft_lsd_pushfront)
* [ft_lsd_pushback](#markdown-header-ft_lsd_pushback)
* [ft_lsd_popfront](#markdown-header-ft_lsd_popfront)
* [ft_lsd_popback](#markdown-header-ft_lsd_popback)
* [Examples](#markdown-header-doubly-linked-lists-example)

[Hash Tables](#markdown-header--hash-tables)
==================
* [Presentation](#markdown-header-hash-tables-presentation)
* [Structure](#markdown-header-hash-tables-structure)
* [ft_htable_create](#markdown-header-ft_htable_create)
* [ft_htable_destroy](#markdown-header-ft_htable_destroy)
* [ft_htable_hash](#markdown-header-ft_htable_hash)
* [ft_htable_setdata](#markdown-header-ft_htable_setdata)
* [ft_htable_getdata](#markdown-header-ft_htable_getdata)
* [Examples](#markdown-header-hash-tables-examples)

[Dynamic Buffers](#markdown-header--dynamic-buffers)
==================
* [Presentation](#markdown-header-dynamic-buffers-presentation)
* [Structure](#markdown-header-dynamic-buffers-structure)
* [ft_dbuf_create](#markdown-header-ft_dbuf_create)
* [ft_dbuf_destroy](#markdown-header-ft_dbuf_destroy)
* [ft_dbuf_write](#markdown-header-ft_dbuf_write)
* [ft_dbuf_write_str](#markdown-header-ft_dbuf_write_str)
* [ft_dbuf_set](#markdown-header-ft_dbuf_set)
* [ft_dbuf_bzero](#markdown-header-ft_dbuf_bzero)
* [ft_dbuf_sub](#markdown-header-ft_dbuf_sub)
* [ft_dbuf_dup](#markdown-header-ft_dbuf_dup)


[Circular Buffers](#markdown-header--circular-buffers)
==================
* [Presentation](#markdown-header-circular-buffers-presentation)

[Header Files](#markdown-header--header-files)
==================
* [libftdata.h](#markdown-header-libftdata-header-file)

#**       - Instructions          **#
## Download ##
```
#!shell
git clone https://trapcodien@bitbucket.org/trapcodien/libftv2.git libft
git clone https://trapcodien@bitbucket.org/trapcodien/libftdata.git libftdata
```

## Compilation ##
```
#!shell
make -C libft
make -C libftdata
gcc main.c -L ./libft -L ./libftdata -I ./libft/includes -I ./libftdata/includes -lft -lftdata
```

## First Use ##
```
#!c
#include "libftdata"

int		main(void)
{
	ft_putendl("libft successfully loaded.");
	#ifdef LIBFTDATA_H
		ft_putendl("libftdata successfully loaded.")
	#endif
	return (0);
}
```




#**       - Stacks          **#

## Stacks Presentation ##
Stacks functions allow **LIFO** (Last-In-First-Out) manipulation on data.

Please take care of this :

* **next** pointer allows you to down in the stack.
* If you want store data in a t_stack, you must must have an inherited t_stack.

For inherit a **t_stack** struct, you have 2 solutions :

```
#!c
typedef struct				s_my_herited_stack
{
	t_stack						private;
	char						*user_data1;
	int							user_data2;
	long						user_data3;
}							t_my_herited_stack;
```

**OR**

```
#!c
typedef struct				s_my_herited_stack
{
	struct s_my_herited_stack	*next;
	char						*user_data1;
	int							user_data2;
	long						user_data3;
}							t_my_herited_stack;
```

## Stacks Structure ##
```
#!c
typedef struct		s_stack
{
	struct s_stack	*next;
}					t_stack;
```

## ft_stack_create ##
```
#!c
t_stack				*ft_stack_create(void *f_construct);
```
**ft_stack_create()** function create a new stack element.

**f_construct** => void \*(\*f_construct)(void)

if **f_construct** is set, sizeof(t_stack) is allocated.

**return** : new element pointer. if alloction fails,
**NULL** is returned.

## ft_stack_destroy ##
```
#!c
void				ft_stack_destroy(void *a_elem, void *f_destruct);
```
**ft_stack_destroy()** function derstroy a **t_stack**.

**f_destruct** => void (\*f_construct)(void \*)

* **a_elem** must be a t_stack\*\* pointer.
* If **f_destruct** is set, it is called before free allocated **t_stack**.

## ft_stack_push ##
```
#!c
void				ft_stack_push(void *a_stack, void *new_elem)
```
**ft_stack_push()** function push **new_elem** on current stack.

* **a_stack** must be a t_stack\*\* pointer. it is head of the stack.
* **new_elem** is a pointer on a new t_stack\* struct.

## ft_stack_del ##
```
#!c
void				ft_stack_del(void *a_stack, void *elem, void *f_destruct);
```
**ft_stack_del()** function delete an **elem** on current stack. it is destroy with **f_destruct** and ft_stack_destroyi() function ().

**f_destruct** => void (\*f_construct)(void \*)

* **a_stack** must be a t_stack\*\* pointer. it is head of the stack.
* **elem** is a t_stack\* pointer which should be deleted.
* If **f_destruct** is set, is is called before free allocated **t_stack**.

## ft_stack_pop ##
```
#!c
void				ft_stack_pop(void *a_stack, void *f_destruct);
```
**ft_stack_pop()** function unstack and destroy an element from **\*a_stack**.

**f_destruct** => void (\*f_construct)(void \*)

* **a_stack** must be a t_stack\*\* pointer. it is head of the stack.
* If **f_destruct** is set, is is called before free allocated **t_stack**.

## ft_stack_debug ##
```
#!c
void				ft_stack_debug(void *stack, void *f_display);
```
**ft_stack_debug()** function allows you to display a list.

**f_display** => void (\*f_display)(void \*)

* **stack** is head stack pointer.
* **f_display** is the function which called for every element of the stack.




#**       - Queues          **#

## Queues Presentation ##
Queues functions allow **FIFO** (First-In-First-Out) manipulation on data.

Please take care of this :

* **next** pointer allows you to down in the queue.
* **tail** pointer allows you to access to the tail of the queue.
* If you want store data in a t_queue, you must have an inherited t_queue.

For inherit a **t_queue** struct, you have 2 solutions :

```
#!c
typedef struct				s_my_herited_queue
{
	t_queue						private;
	char						*user_data1;
	int							user_data2;
	long						user_data3;
}							t_my_herited_queue;
```

**OR**

```
#!c
typedef struct				s_my_herited_queue
{
	struct s_my_herited_queue	*next;
	struct s_my_herited_queue	*tail;
	char						*user_data1;
	int							user_data2;
	long						user_data3;
}							t_my_herited_queue;
```

## Queues Structure ##
```
#!c
typedef struct		s_queue
{
	struct s_queue	*next;
	struct s_queue	*tail;
}					t_queue;
```

## ft_queue_create ##
```
#!c
t_queue				*ft_queue_create(void *f_construct);
```
**ft_queue_create()** function create a new queue element.

**f_construct** => void \*(\*f_construct)(void)

if **f_construct** is set, sizeof(t_queue) is allocated.

**return** : new element pointer. if alloction fails,
**NULL** is returned.

## ft_queue_destroy ##
```
#!c
void				ft_queue_destroy(void *a_elem, void *f_destruct);
```
**ft_queue_destroy()** function derstroy a **t_queue**.

**f_destruct** => void (\*f_construct)(void \*)

* **a_elem** must be a t_queue\*\* pointer.
* If **f_destruct** is set, it is called before free allocated **t_queue**.

## ft_queue_push ##
```
#!c
void				ft_queue_push(void *a_queue, void *new_elem)
```
**ft_queue_push()** function push **new_elem** on current queue.

* **a_queue** must be a t_queue\*\* pointer. it is head of the queue.
* **new_elem** is a pointer on a new t_queue\* struct.

## ft_queue_pop ##
```
#!c
void				ft_queue_pop(void *a_queue, void *f_destruct);
```
**ft_queue_pop()** function unqueue and destroy an element from **\*a_queue**.

**f_destruct** => void (\*f_construct)(void \*)

* **a_queue** must be a t_queue\*\* pointer. it is head of the queue.
* If **f_destruct** is set, is is called before free allocated **t_queue**.





#**       - Doubly Linked Lists          **#

## Doubly Linked Lists Presentation ##
LSD implements basic doubly linked lists.

Please take care of this :

* **t_lsd** represents the main struct of the list. that allows you to access
to thes **head** or the **tail**.
* **t_elsd** represents a link of the list. **next** and **prev** allows you
to navigate into the linked list.
* If you want store data in a **t_elsd**, you must have an inherited **t_elsd**.

For inherit a **t_elsd** struct, you have 2 solutions :

```
#!c
typedef struct				s_my_herited_elsd
{
	t_elsd						private;
	char						*user_data1;
	int							user_data2;
	long						user_data3;
}							t_my_herited_elsd;
```

**OR**

```
#!c
typedef struct				s_my_herited_elsd
{
	struct s_my_herited_elsd	*next;
	struct s_my_herited_elsd	*tail;
	char						*user_data1;
	int							user_data2;
	long						user_data3;
}							t_my_herited_elsd;
```

## Doubly Linked Lists Structures ##
```
#!c
typedef struct		s_lsd
{
	void			*head;
	void			*tail;
}					t_lsd;
```
```
#!c
typedef struct		s_elsd
{
	struct s_elsd	*next;
	struct s_elsd	*prev;
}					t_elsd;
```

## Doubly Linked Lists Schema ##
![doubly linked lists schema](http://image.noelshack.com/fichiers/2014/48/1417206581-t-lsd-schema.png "Schema")


## ft_lsd_create ##
```
#!c
t_lsd				*ft_lsd_create(void);
```
**ft_lsd_create()** function allocates a new **t_lsd** struct.

**return** : new fresh **t_lsd** pointer, or NULL if allocation fails.

## ft_lsd_destroy ##
```
#!c
void				ft_lsd_destroy(t_lsd **lsd);
```
**ft_lsd_destroy()** function destroy a **t_lsd** struct and set **\*lsd**
pointer to **NULL**.


## ft_lsd_insert_left ##
```
#!c
void				ft_lsd_insert_left(t_lsd *lsd, void *new_elsd, void *elsd);
```
**ft_lsd_insert_left()** function inserts **new_elsd** at the left of **elsd**
in **lsd** list.

* Note : **new_elsd** must be allocated by the user.


## ft_lsd_insert_right ##
```
#!c
void				ft_lsd_insert_right(t_lsd *lsd, void *elsd, void *new_elsd);
```
**ft_lsd_insert_right()** function inserts **new_elsd** at the right of **elsd**
in **lsd** list.

* Note : **new_elsd** must be allocated by the user.

## ft_lsd_pick ##
```
#!c
void				*ft_lsd_pick(t_lsd *lsd, void *a_elsd);
```

**ft_lsd_pick()** function allows you to pick an element from a list.

* **lsd** is a pointer to the **t_lsd** struct.
* **a_elsd** represents a **t_elsd\*\*** struct.

* Note : **t_elsd \*\*elem** pointer is set to next element.
if next is **NULL**, **\*elem** is set to prev.
if **\*elem** is last element of the list, **\*elem** is set to **NULL**.

## ft_lsd_pushfront ##
```
#!c
void				ft_lsd_pushfront(t_lsd *lsd, void *new_elsd);
```

**ft_lsd_pushfront()** allows you to handle your list like a stack (FIFO).
It pushes **new_elsd** in the list.

* **lsd** is a **t_lsd** struct pointer.
* **new_elsd** is the new allocted **t_lsd\*** pointer.

## ft_lsd_pushback ##
```
#!c
void				ft_lsd_pushback(t_lsd *lsd, void *new_elsd);
```

**ft_lsd_pushback()** allows you to handle your list like a queue (LIFO).
It pushes **new_elsd** in the list.

* **lsd** is a **t_lsd** struct pointer.
* **new_elsd** is the new allocted **t_lsd\*** pointer.

## ft_lsd_popfront ##
```
#!c
void				*ft_lsd_popfront(t_lsd *lsd);
```

**ft_lsd_popfront** allows you to handle youe list like a stack (FIFO).
It pops a **t_elsd** element.

* **lsd** is a **t_lsd** struct pointer.
* **return** : popped **t_elsd** struct pointer.

## ft_lsd_popback ##
```
#!c
void				*ft_lsd_popback(t_lsd *lsd);
```

**ft_lsd_popback** allows you to handle youe list like a queue (LIFO).
It pops a **t_elsd** element.

* **lsd** is a **t_lsd** struct pointer.
* **return** : popped **t_elsd** struct pointer.


## Doubly Linked Lists Example ##
```
#!c
/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: garm <garm@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/26 00:46:43 by garm              #+#    #+#             */
/*   Updated: 2014/11/28 21:01:59 by garm             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libftdata.h"

/* Inherited t_elsd struct */

typedef struct		s_elem
{
	struct s_elem	*next;
	struct s_elem	*prev;
	char			*str;
}					t_elem;

t_elem		*ft_create_elem(char *str)
{
	t_elem	*new;

	new = (t_elem *)ft_memalloc(sizeof (t_elem));
	if (!new)
		return (NULL);
	new->str = ft_strdup(str);
	return (new);
}

void		ft_destroy_elem(t_elem *elem)
{
	if (elem)
		ft_strdel(&elem->str);
	ft_memdel((void **)&elem);
}

void		ft_destroy_list(t_lsd *lsd)
{
	t_elem	*cursor;

	if (!lsd)
		return ;
	while (lsd->head)
	{
		cursor = lsd->head;
		cursor = ft_lsd_pick(lsd, (void **)&(cursor));
		ft_destroy_elem(cursor);
	}
	ft_lsd_destroy(&lsd);
}

void	display_list(t_lsd *lsd, t_elem *cursor)
{
	t_elem	*tmp;

	if (!lsd)
		return ;
	tmp = lsd->head;
	while (tmp)
	{
		if (tmp == lsd->head)
			ft_putstr("[head]");
		if (tmp == cursor)
			ft_putstr("*");
		ft_putstr(tmp->str);
		if (tmp == cursor)
			ft_putstr("*");
		if (tmp == lsd->tail)
			ft_putstr("[tail]");
		if (tmp->next)
			ft_putstr(" <=> ");
		else
			ft_putstr("\n");
		tmp = tmp->next;
	}
}

int		main(void)
{
	char	*line;
	t_lsd	*lsd;
	t_elem	*cursor;
	t_elem	*new;

	cursor = NULL;
	lsd = ft_lsd_create();
	while (ft_putstr("LSD> ") && get_next_line(0, &line) > 0)
	{
		if (!ft_strncmp(line, "pushleft ", 9))
		{
			new = ft_create_elem(line + 9);
			ft_lsd_insert_left(lsd, new, cursor);
			if (!cursor)
				cursor = new;
		}
		else if (!ft_strncmp(line, "pushright ", 10))
		{
			new = ft_create_elem(line + 10);
			ft_lsd_insert_right(lsd, cursor, new);
			if (!cursor)
				cursor = new;
		}
		else if (!ft_strncmp(line, "pushfront ", 10))
		{
			new = ft_create_elem(line + 10);
			ft_lsd_pushfront(lsd, new);
			if (!cursor)
				cursor = new;
		}
		else if (!ft_strncmp(line, "pushback ", 9))
		{
			new = ft_create_elem(line + 9);
			ft_lsd_pushback(lsd, new);
			if (!cursor)
				cursor = new;
		}
		else if (!ft_strncmp(line, "popfront", 8))
		{
			new = ft_lsd_popfront(lsd);
			ft_destroy_elem(new);
		}
		else if (!ft_strncmp(line, "popback", 7))
		{
			new = ft_lsd_popback(lsd);
			ft_destroy_elem(new);
		}
		else if (!ft_strncmp(line, "pick", 4))
		{
			new = ft_lsd_pick(lsd, &cursor);
			ft_destroy_elem(new);
		}
		else if (!ft_strncmp(line, "left", 4))
		{
			if (cursor && cursor->prev)
				cursor = cursor->prev;
		}
		else if (!ft_strncmp(line, "right", 5))
		{
			if (cursor && cursor->next)
				cursor = cursor->next;
		}
		else
			ft_putendl("\n      Help :\n\tleft\n\tright\n\tpick\n\tpushleft\n\tpushright\n\tpushfront\n\tpushback\n\tpopfront\n\tpopback\n");
		ft_putchar('\n');
		display_list(lsd, cursor);
		ft_strdel(&line);
	}
	ft_destroy_list(lsd);
	return (0);
}
```


#**       - Hash Tables          **#

## Hash Tables Presentation ##

Hash Tables functions allow you to quickly store and search any data with an
unique **id** key (char * value)
if a new element is created with the same **id** key, data is simply replaced.
Collisions are managed with basic non-equilibrated binary tree.

Please take care of this :

* **left** pointer allows you to go to the left branch.
* **right** pointer allows you to go to the right branch.
* **data** pointer is your responsability.
* **id** is the key that identfy the corresponding element in the hash table.

## Hash Tables Structure ##
**s_ht** means Hash Table Structure.
```
#!c
typedef struct		s_ht
{
	char			*id;
	void			*data;
	struct s_ht	 	*left;
	struct s_ht	 	*right;
}					t_ht;
```

## ft_htable_create ##

```
#!c
t_ht				**ft_htable_create(void);
```

**ft_htable_create()** function simply allocates a new array of **HTABLE_SIZE**
pointers. 

**return** : fresh pointer to the hash table of (**HTABLE_SIZE** \* 8) bytes.

Note : **HTABLE_SIZE** macro can be found in libftdata.h header file.

## ft_htable_destroy ##

```
#!c
void				ft_htable_destroy(t_ht **hashtable)
```

**ft_htable_destroy()** function **free** all elements in the hash table.

For all elements in the hashtable :

* **id** is **free**;
* **data** pointer is set to **NULL** but **data** corresponding bytes stay
intact.

## ft_htable_hash ##

```
#!c
int					ft_htable_hash(char *id);
```

**ft_htable_hash()** function is a crappy basic hash function, but strangly
efficient because it uses **SALT** (see libftdata.h header file for
more details).

**return** : generated fingerprint based on **id** key.


## ft_htable_setdata ##

```
#!c
void				ft_htable_setdata(t_ht **hashtable, char *id, void *data);
```

**ft_htable_setdata()** function allows you to store **data** pointer
in a given identified **id** area.


## ft_htable_getdata ##

```
#!c
void				*ft_htable_getdata(t_ht **hashtable, char *id)
```

**ft_htable_getdata()** function search on a given **id** in **hashtable**.

**return** : **data** pointer is returned. if nothing is found, NULL is
returned.

## Hash Tables Examples ##

### Example 1 ###

```
#!c
#include "libftdata.h"

int			main(void)
{
	char	hello[] = "Hello World !";
	char	bonjour[] = "Bonjour tout le monde !";
	t_ht	**table;

/* Hash table creation */
	table = ft_htable_create();

/* Store hello pointer in "AAA" */
	ft_htable_setdata(table, "AAA", hello);

/* Store bonjour pointer in "BBB" */
	ft_htable_setdata(table, "BBB", bonjour);

/* replace hello pointer by bonjour pointer in "AAA" */
	ft_htable_setdata(table, "AAA", bonjour);

	ft_putendl(ft_htable_getdata(table, "AAA")); /* print bonjour */
	ft_putendl(ft_htable_getdata(table, "BBB")); /* print bonjour */
	ft_putendl(ft_htable_getdata(table, "CCC")); /* here, data is null */

	ft_htable_setdata(table, "CCC", hello);
	ft_putendl(ft_htable_getdata(table, "CCC")); /* print hello */

/* Hash table destruction */
	ft_htable_destroy(table);
	return (0);
}
```

#**       - Dynamic Buffers          **#

## Dynamic Buffers Presentation ##

**dbuf** is a basic implementation of Dynamic Allocated Buffers.
The **size** of **data** is automatically **resize** when **offset** is out of data.

Please take care of this :

* **data** pointer allows you to access to the dynamic buffer memory.
* **resize** value allows you to choose how many bytes the **dbuf** will be resize.
* **size** value is the size of **data**.
* **offset** is a virtual cursor that determines the reading head (used by **ft_dbuf_write**, **ft_dbuf_write_str**, **ft_dbuf_set** and **ft_dbuf_sub**), this value can be negative, **dbuf** will always be resize properly.

## Dynamic Buffers Structure ##
```
#!c
typedef struct		s_dbuf
{
	void			*data;
	size_t			resize;
	size_t			size;
	long			offset;
}					t_dbuf;
```

## ft_dbuf_create ##
```
#!c
t_dbuf				*ft_dbuf_create(size_t resize);
```

**ft_dbuf_create()** function allocate a new dynamic buffer of **resize** bytes.

**return** : fresh pointer to the **dbuf**.

## ft_dbuf_destroy ##
```
#!c
void				ft_dbuf_destroy(t_dbuf **b);
```

**ft_dbuf_destroy()** function allows you to free a dynamic buffer.

## ft_dbuf_write ##
```
#!c
int					ft_dbuf_write(t_dbuf *b, void *data, size_t len);
```

**ft_dbuf_write()** function allow you to write **len** bytes of **data** in **b** from **offset**.

**return** : number of **bytes** written.

## ft_dbuf_write_str ##
```
#!c
int					ft_dbuf_write_str(t_dbuf *b, const char *str);
```

**ft_dbuf_write_str()** function allow you to write the string **str** in dbuf **b** from **offset**.

**return** : number of **bytes** written.

## ft_dbuf_set ##
```
#!c
int					ft_dbuf_set(t_dbuf *b, char c, long shift, size_t len);
```

**ft_dbuf_set()** is a **memset** like function for dynamic buffer.
It fills **len** bytes from **offset** induced by **shift**.
**shift** can be negative.

**return** : number of **bytes** written.

## ft_dbuf_bzero ##
```
#!c
void				ft_dbuf_bzero(t_dbuf *b);
```

**ft_dbuf_bzero()** is a **bzero** like function for dynamic buffer.
It fills the buffer of zero.

## ft_dbuf_sub ##
```
#!c
t_dbuf				*ft_dbuf_sub(t_dbuf *b, long shift, size_t len);
```

**ft_dbuf_sub()** allows you to copy a part of **dbuf** from **offset** induced by **shift** and **len**.

**return** : new **dbuf** pointer.

## ft_dbuf_dup ##
```
#!c
t_dbuf				*ft_dbuf_dup(t_dbuf *b);
```

**ft_dbuf_dup()** allows you to duplicate a given **dbuf**.

**return** : new duplicated **dbuf** pointer.

#**       - Circular Buffers          **#

## Circular Buffers Presentation ##

**cbuf** is a basic implementation of Circular Buffers.
The length of the allocated circular buffer is fixed to **size**.



#**       - Header Files          **#

## libftdata header file ##

```
#!c
/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   libftdata.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: garm <garm@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/06/18 23:52:49 by garm              #+#    #+#             */
/*   Updated: 2014/11/28 20:51:38 by garm             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LIBFTDATA_H
# define LIBFTDATA_H

# include "libft.h"

# define S_P1 "L0KiPEwowXmGbW1Q8v-sNg1e6GaAcgh4lF9-t5IkqBpr5WYc7AQ7jHOyn3bCUdL4"
# define S_P2 "BhMaCEDJu2Vfs_6SdjZkrTexJY8R0PqOTnHtSyF3Nfi9pVMzIUxolZRzuX2_mKvD"
# define HTABLE_SALT S_P1 S_P2
# define HTABLE_SIZE (256 * 256)

# define DEFAULT_RESIZE 32

# define CBUF_EMPTY 0
# define CBUF_NORMAL 1
# define CBUF_FULL 2

# define CBUF_GET_DISTANCE(s, d, size) ((d >= s) ? (d - s) : (size + (d - s)))

typedef void		*(*t_data_construct)(void);
typedef void		(*t_data_destruct)(void *);
typedef void		(*t_data_display)(void *);

typedef struct		s_stack
{
	struct s_stack	*next;
}					t_stack;

typedef struct		s_queue
{
	struct s_queue	*next;
	struct s_queue	*tail;
}					t_queue;

typedef struct		s_elsd
{
	struct s_elsd	*next;
	struct s_elsd	*prev;
}					t_elsd;

typedef struct		s_lsd
{
	void			*head;
	void			*tail;
}					t_lsd;

typedef struct		s_ht
{
	char			*id;
	void			*data;
	struct s_ht		*left;
	struct s_ht		*right;
}					t_ht;

typedef struct		s_dbuf
{
	void			*data;
	size_t			resize;
	size_t			size;
	long			offset;
}					t_dbuf;

typedef struct		s_cbuf
{
	void			*data;
	size_t			size;
	off_t			off_c;
	off_t			off_p;
	char			state;
}					t_cbuf;

/*
** Stacks
*/
void				*ft_stack_create(void *f_construct);
void				ft_stack_destroy(void *a_elem, void *f_destruct);
void				ft_stack_push(void *a_stack, void *new_elem);
void				ft_stack_pop(void *a_stack, void *f_destruct);
void				ft_stack_debug(void *stack, void *f_display);

/*
** Queues
*/
void				*ft_queue_create(void *f_construct);
void				ft_queue_destroy(void *a_elem, void *f_destruct);
void				ft_queue_push(void *a_queue, void *new_elem);
void				ft_queue_pop(void *a_queue, void *f_destruct);
void				ft_queue_debug(void *queue, void *f_display);

/*
** LSD is not a drug, just double linked lists implementation
*/
t_lsd				*ft_lsd_create(void);
void				ft_lsd_destroy(t_lsd **lsd);
void				ft_lsd_insert_left(t_lsd *lsd, void *new_elsd, void *elsd);
void				ft_lsd_insert_right(t_lsd *lsd, void *elsd, void *new_elsd);
void				*ft_lsd_pick(t_lsd *lsd, void *a_elsd);
void				ft_lsd_pushfront(t_lsd *lsd, void *new_elsd);
void				ft_lsd_pushback(t_lsd *lsd, void *new_elsd);
void				*ft_lsd_popfront(t_lsd *lsd);
void				*ft_lsd_popback(t_lsd *lsd);

/*
** Hash Tables (collisions managed with binary trees)
*/
t_ht				**ft_htable_create(void);
void				ft_htable_destroy(t_ht **hashtable);
int					ft_htable_hash(char *id);
void				*ft_htable_getdata(t_ht **hashtable, char *id);
void				ft_htable_setdata(t_ht **hashtable, char *id, void *data);

/*
** Dynamic Buffers
*/
t_dbuf				*ft_dbuf_create(size_t resize);
void				ft_dbuf_destroy(t_dbuf **b);
int					ft_dbuf_write(t_dbuf *b, void *data, size_t len);
int					ft_dbuf_write_str(t_dbuf *b, const char *str);
int					ft_dbuf_set(t_dbuf *b, char c, long shift, size_t len);
void				ft_dbuf_bzero(t_dbuf *b);
t_dbuf				*ft_dbuf_sub(t_dbuf *b, long shift, size_t len);
t_dbuf				*ft_dbuf_dup(t_dbuf *b);

/*
** Circular Buffer
*/
t_cbuf				*ft_cbuf_create(size_t size);
void				ft_cbuf_destroy(t_cbuf **b);
ssize_t				ft_cbuf_read(int fd, t_cbuf *cbuf, size_t nbytes);
ssize_t				ft_cbuf_produce(t_cbuf *cbuf, void *data, size_t size);
ssize_t				ft_cbuf_write(int fd, t_cbuf *cbuf, size_t nbytes);
ssize_t				ft_cbuf_write_noflush(int fd, t_cbuf *cbuf, size_t nbytes);
ssize_t				ft_cbuf_consume(t_cbuf *cbuf, void *data, size_t size);
ssize_t				ft_cbuf_consume_noflush(t_cbuf *b, void *d, size_t size);
size_t				ft_cbuf_get_distance(t_cbuf *cbuf, size_t src, size_t dst);
ssize_t				ft_cbuf_findchr(t_cbuf *cbuf, char c, off_t o, size_t len);
ssize_t				ft_cbuf_findrchr(t_cbuf *cbuf, char c, off_t o, size_t len);
int					ft_cbuf_debug(t_cbuf *cbuf);

#endif
```

