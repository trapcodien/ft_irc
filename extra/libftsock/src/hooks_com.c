/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   hooks_com.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: garm <garm@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/10/16 02:52:59 by garm              #+#    #+#             */
/*   Updated: 2015/04/06 00:50:09 by garm             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libftsock.h"

void	disconnect_hook(t_com *c, void *f)
{
	if (c)
		c->disconnect_handle = f;
}

void	recv_hook(t_com *c, void *f)
{
	if (c)
		c->recv_handle = f;
}

void	ack_hook(t_com *c, void *f)
{
	if (c)
		c->ack_handle = f;
}
