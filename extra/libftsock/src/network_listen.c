/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   network_listen.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: garm <garm@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/12 07:04:37 by garm              #+#    #+#             */
/*   Updated: 2015/04/01 16:44:02 by garm             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libftsock.h"

t_port		*network_listen(t_net *net, int port, int backlog, void *construct)
{
	t_port				*server;
	t_port_construct	c;

	if (!net)
		return (NULL);
	if ((c = construct))
		server = c();
	else
		server = ft_memalloc(sizeof(t_port));
	if (!server)
		return (NULL);
	server->tcps = ftsock_create(FTSOCK_SERVER, port, backlog);
	if (server->tcps->error)
	{
		ftsock_destroy(&server->tcps);
		ft_memdel((void **)&server);
		return (NULL);
	}
	server->net = net;
	list_push(&(net->servers), server);
	return (server);
}
