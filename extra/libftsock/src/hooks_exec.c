/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   hooks_exec.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: garm <garm@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/30 19:35:13 by garm              #+#    #+#             */
/*   Updated: 2014/11/30 19:43:12 by garm             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libftsock.h"

void		exec_hook(t_net *n, void *func, void *exec_constructor)
{
	if (n)
	{
		n->exec_handle = func;
		n->exec_constructor = exec_constructor;
	}
}

void		exec_return_hook(t_net *n, void *func)
{
	if (n)
		n->exec_return_handle = func;
}
