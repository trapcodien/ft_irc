/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   network_utils.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: garm <garm@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/15 23:53:30 by garm              #+#    #+#             */
/*   Updated: 2014/12/10 22:25:51 by garm             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libftsock.h"

void			network_enable_ncurses_callback(t_net *net)
{
	net->ncurses = 1;
}

int				network_is_server(t_port *port)
{
	if (!port)
		return (ERROR);
	return ((port->tcps) ? (1) : 0);
}

void			close_com(t_com *com)
{
	t_port		*port;

	if (com && com->closed && com->port && com->port->clients && com->port->net)
	{
		port = com->port;
		if (!port->send)
		{
			if (com->tcps)
			{
				FD_CLR(com->tcps->sock, &com->port->net->write_fds);
				ftsock_destroy(&(com->tcps));
			}
			cbuff_destroy(&com->recv.cbuf);
			list_del(&(port->clients), com, NULL);
		}
	}
}

void			close_port(t_port *port)
{
	t_port		**head;
	t_com		*com;

	if (port && port->net && (port->closed || (!port->clients && !port->tcps)))
	{
		if (network_is_server(port))
			head = &port->net->servers;
		else
			head = &port->net->connections;
		com = port->clients;
		while (com)
		{
			close_com(com);
			com = com->next;
		}
		if (port && port->tcps)
			ftsock_destroy(&(port->tcps));
		if (!port->clients)
		{
			while (utils_queues_pop(&(port->send)))
				;
			list_del(head, port, NULL);
		}
	}
}
