/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   network_refresh.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: garm <garm@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/15 23:57:11 by garm              #+#    #+#             */
/*   Updated: 2014/12/04 23:39:33 by garm             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libftsock.h"

void				refresh_com_higher_fd(t_com *com)
{
	t_net			*net;

	if (!com || !com->port || !com->port->net || !com->tcps || com->closed)
		return ;
	net = com->port->net;
	FD_SET(com->tcps->sock, &net->read_fds);
	if (com->tcps->sock > net->higher_fd)
		net->higher_fd = com->tcps->sock;
}

void				refresh_port_higher_fd(t_port *port)
{
	t_net			*net;

	if (!port || !port->net || !port->tcps || port->closed)
		return ;
	net = port->net;
	FD_SET(port->tcps->sock, &net->read_fds);
	if (port->tcps->sock > net->higher_fd)
		net->higher_fd = port->tcps->sock;
}

int					refresh_connections(t_net *net)
{
	t_port_func		port_funcs[3];
	t_com_func		com_funcs[3];

	if (net)
	{
		net->higher_fd = 1;
		port_funcs[0] = &refresh_port_higher_fd;
		port_funcs[1] = &close_port;
		port_funcs[2] = NULL;
		com_funcs[0] = &refresh_com_higher_fd;
		com_funcs[1] = &close_com;
		com_funcs[2] = NULL;
		FD_ZERO(&net->read_fds);
		if (net->stdin_handle)
			FD_SET(STDIN_FILENO, &net->read_fds);
		network_foreach(net->servers, port_funcs, com_funcs);
		network_foreach(net->connections, port_funcs, com_funcs);
	}
	if (net && (net->servers || net->connections || net->stdin_handle))
		return (1);
	return (0);
}

int					refresh_executions(t_net *net)
{
	t_exec			*e;
	t_exec			*todel;

	if (net && net->executions)
	{
		e = net->executions;
		while (e)
		{
			todel = NULL;
			if (e->pty != ERROR)
			{
				if (e->pty > net->higher_fd)
					net->higher_fd = e->pty;
				FD_SET(e->pty, &net->read_fds);
			}
			else
				todel = e;
			e = e->next;
			if (todel)
				exec_del(&net->executions, todel);
		}
	}
	return (1);
}
