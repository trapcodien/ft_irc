/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   network_create.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: garm <garm@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/12 11:45:15 by garm              #+#    #+#             */
/*   Updated: 2014/11/17 18:25:07 by garm             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libftsock.h"

t_net		*network_create(void *construct)
{
	t_net_construct		c;
	t_net				*network;

	if ((c = construct))
		network = c();
	else
		network = ft_memalloc(sizeof(t_net));
	network->servers = NULL;
	network->connections = NULL;
	FD_ZERO(&network->read_fds);
	FD_ZERO(&network->write_fds);
	network->timeout_handle = NULL;
	network->loop_handle = NULL;
	network->end_loop_handle = NULL;
	network->stdin_handle = NULL;
	network->ncurses = 0;
	return (network);
}
