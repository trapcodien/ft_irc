/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   utils_queues_push.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: garm <garm@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/06/19 03:11:03 by garm              #+#    #+#             */
/*   Updated: 2014/10/28 17:26:37 by garm             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libftsock.h"

t_queues		*utils_queues_push(t_queues *q, void *data, char type)
{
	t_queues	*new;

	new = utils_queues_create(data, type);
	if (!new)
		return (NULL);
	if (q)
	{
		q->tail->next = new;
		q->tail = new;
	}
	else
	{
		q = new;
		q->tail = q;
	}
	return (q);
}
