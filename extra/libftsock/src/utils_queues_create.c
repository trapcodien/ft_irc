/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   utils_queues_create.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: garm <garm@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/06/19 03:08:31 by garm              #+#    #+#             */
/*   Updated: 2014/10/28 17:26:21 by garm             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libftsock.h"

t_queues		*utils_queues_create(void *data, char type)
{
	t_queues	*q;

	q = (t_queues *)ft_memalloc(sizeof(t_queues));
	if (!q)
		return (NULL);
	q->data = data;
	q->next = NULL;
	q->tail = NULL;
	q->type = type;
	return (q);
}
