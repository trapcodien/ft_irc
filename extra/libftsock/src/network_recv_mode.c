/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   network_recv_mode.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: garm <garm@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/10/28 08:28:13 by garm              #+#    #+#             */
/*   Updated: 2014/11/25 03:31:02 by garm             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libftsock.h"

void		network_recv_mode(t_com *c, char mode)
{
	if (c && (mode == MODE_RAW || mode == MODE_TEXT || mode == MODE_SIZE))
		c->recv.mode = mode;
	if (c && mode == MODE_SIZE && !c->recv.size)
		network_recv_mode_setsize(c, 0);
}

void		network_recv_mode_setsize(t_com *c, size_t size)
{
	if (c)
	{
		if (!size)
			size = DEFAULT_MODE_SIZE;
		c->recv.size = size;
		c->recv.rbytes = size;
	}
}
