/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   network_exec.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: garm <garm@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/30 19:45:16 by garm              #+#    #+#             */
/*   Updated: 2014/12/05 00:16:21 by garm             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include "libftsock.h"

static void		set_pty(t_exec *e, int master, int slave)
{
	close(slave);
	e->pty = master;
}

static void		dup_pty(int master, int slave)
{
	close(master);
	login_tty(slave);
}

t_exec			*network_exec(t_net *net, char *path, char **argv, char **env)
{
	int		master;
	int		slave;
	t_exec	*e;

	if (!net || !path)
		return (NULL);
	if (net->exec_constructor)
		e = net->exec_constructor();
	else
		e = (t_exec *)ft_memalloc(sizeof(t_exec));
	e->net = net;
	openpty(&master, &slave, NULL, NULL, NULL);
	if ((e->child = fork()))
		set_pty(e, master, slave);
	else if (!e->child)
	{
		dup_pty(master, slave);
		execve(path, argv, env);
		exit(1);
	}
	else
		ft_memdel((void **)&e);
	list_push(&(net->executions), e);
	return (e);
}

ssize_t			network_exec_write(t_exec *e, void *buffer, size_t size)
{
	if (e && e->pty != ERROR)
		return (write(e->pty, buffer, size));
	return (ERROR);
}
