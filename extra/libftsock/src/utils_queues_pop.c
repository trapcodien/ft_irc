/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   utils_queues_pop.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: garm <garm@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/06/19 03:23:23 by garm              #+#    #+#             */
/*   Updated: 2014/10/30 05:48:14 by garm             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libftsock.h"

void		*utils_queues_pop(t_queues **q)
{
	void		*data;
	t_queues	*todel;

	if (!q || !*q)
		return (NULL);
	todel = *q;
	if (todel->next)
	{
		(*q) = (*q)->next;
		(*q)->tail = todel->tail;
	}
	else
		*q = NULL;
	data = todel->data;
	ft_memdel((void **)&todel);
	return (data);
}
