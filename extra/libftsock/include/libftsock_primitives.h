/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   libftsock_primitives.h                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: garm <garm@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/11/30 19:15:26 by garm              #+#    #+#             */
/*   Updated: 2014/11/30 19:20:44 by garm             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LIBFTSOCK_PRIMITIVES_H
# define LIBFTSOCK_PRIMITIVES_H

# include <sys/types.h>
# include <sys/socket.h>
# include <sys/wait.h>
# include <netinet/in.h>
# include <arpa/inet.h>
# include <netdb.h>

# define FTSOCK_SERVER 0
# define FTSOCK_CLIENT 1

# define FTSOCK_NOERROR 0
# define FTSOCK_SOCKETERROR 1
# define FTSOCK_BINDERROR 2
# define FTSOCK_LISTENERROR 3
# define FTSOCK_ACCEPTERROR 4
# define FTSOCK_CONNECTERROR 5
# define FTSOCK_MUSTBE_CLIENT 6
# define FTSOCK_MUSTBE_SERVER 7
# define FTSOCK_UNABLE_RESOLVE 8
# define FTSOCK_BAD_PORT 9
# define FTSOCK_SETSOCK_ERROR 10

# define FTSOCK_SOCKETERROR_STR "ftsock: socket() error."
# define FTSOCK_BINDERROR_STR "ftsock: bind() error."
# define FTSOCK_LISTENERROR_STR "ftsock: listen() error."
# define FTSOCK_ACCEPTERROR_STR "ftsock: accept() error."
# define FTSOCK_CONNECTERROR_STR "ftsock: connect() error."
# define FTSOCK_MUSTBE_CLIENT_STR "ftsock error: type must be FTSOCK_CLIENT."
# define FTSOCK_MUSTBE_SERVER_STR "ftsock error: type must be FTSOCK_SERVER."
# define FTSOCK_UNABLE_RESOLVE_STR "ftsock error: Unable to resolve host."
# define FTSOCK_BAD_PORT_STR "ftsock error: Bad port."
# define FTSOCK_SETSOCK_ERROR_STR "ftsock error : setsockopt() error."

#endif
