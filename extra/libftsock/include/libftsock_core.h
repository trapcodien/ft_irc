/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   libftsock_core.h                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: garm <garm@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/06/28 01:04:34 by garm              #+#    #+#             */
/*   Updated: 2014/12/14 21:50:46 by garm             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LIBFTSOCK_CORE_H
# define LIBFTSOCK_CORE_H

# define SINSIZE sizeof(struct sockaddr_in)

# define CBUFF_EMPTY 0
# define CBUFF_NORMAL 1
# define CBUFF_FULL 2

# define FTSOCK_DIFFUSE 0
# define FTSOCK_PACKET 1

typedef struct timeval		t_timeval;
typedef struct sockaddr		t_sockaddr;
typedef struct protoent		t_protoent;
typedef struct s_tcpsock	t_sock;
typedef struct s_network	t_net;
typedef struct s__port		t_port;
typedef struct s__com		t_com;
struct s_exec;

typedef t_net				*(*t_net_construct)(void);
typedef t_port				*(*t_port_construct)(void);
typedef t_com				*(*t_com_construct)(void);
typedef struct s_exec		*(*t_exec_construct)(void);
typedef void				(*t_port_func)(t_port *);
typedef void				(*t_com_func)(t_com *);
typedef void				(*t_ncurses_callback)(void *, int);

typedef struct				s_cbuff
{
	void					*data;
	size_t					size;
	off_t					off_c;
	off_t					off_p;
	char					state;
}							t_cbuff;

typedef struct				s_tcpsock
{
	struct sockaddr_in		sin;
	int						sock;
	char					*ip;
	int						port;
	int						backlog;
	char					type;
	char					error;
}							t_tcpsock;

typedef struct				s_recv
{
	t_cbuff					*cbuf;
	char					mode;
	size_t					size;
	size_t					rbytes;
}							t_recv;

typedef struct				s_queues
{
	void					*data;
	struct s_queues			*next;
	struct s_queues			*tail;
	t_com					*emit;
	t_com					*dest;
	char					type;
	char					ack;
}							t_queues;

typedef struct				s_diffuse
{
	off_t					offset;
	size_t					len;
	t_cbuff					*cbuf;
}							t_diffuse;

typedef struct				s_packet
{
	void					*ptr;
	size_t					written_bytes;
	size_t					size;
}							t_packet;

typedef struct				s__com
{
	struct s__com			*next;
	t_sock					*tcps;
	t_recv					recv;
	void					(*disconnect_handle)(t_com *);
	size_t					(*recv_handle)(t_com *, void *, size_t);
	void					(*ack_handle)(t_com *, void *, size_t);
	char					ack;
	t_port					*port;
	char					closed;
}							t__com;

typedef struct				s__port
{
	struct s__port			*next;
	t_sock					*tcps;
	t_com					*clients;
	t_queues				*send;
	t_net					*net;
	t_com_construct			com_constructor;
	void					(*connect_handle)(t_com *);
	void					(*closeport_handle)(t_port *);
	char					closed;
}							t__port;

typedef struct				s_exec
{
	struct s_exec			*next;
	pid_t					child;
	int						pty;
	char					buffer[FTSOCK_EXEC_BUF + 1];
	t_net					*net;
}							t_exec;

typedef struct				s_network
{
	t_port					*servers;
	t_port					*connections;
	t_exec					*executions;
	t_exec_construct		exec_constructor;
	void					(*exec_handle)(t_exec *, void *, size_t);
	void					(*exec_return_handle)(t_exec *, int);
	fd_set					read_fds;
	fd_set					write_fds;
	int						higher_fd;
	void					(*timeout_handle)(t_net *);
	void					(*loop_handle)(t_net *);
	void					(*end_loop_handle)(t_net *);
	int						ncurses;
	void					(*stdin_handle)(t_net *, void *, size_t);
	struct timeval			tv;
	char					stdin_buffer[FTSOCK_STDIN_BUF + 1];
}							t_network;

#endif
