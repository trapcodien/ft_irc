/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memset.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: garm <garm@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/06/15 17:11:33 by garm              #+#    #+#             */
/*   Updated: 2014/09/23 12:07:02 by garm             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void		*ft_memset(void *b, int c, size_t len)
{
	size_t		i;
	char		*memory;

	if (!b)
		return (NULL);
	memory = (char *)b;
	i = 0;
	while (i < len)
	{
		memory[i] = c;
		i++;
	}
	return (b);
}
