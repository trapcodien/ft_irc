/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putendl_fd.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: garm <garm@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/06/16 20:30:24 by garm              #+#    #+#             */
/*   Updated: 2014/06/16 23:57:21 by garm             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int		ft_putendl_fd(const char *str, int fd)
{
	int		ret;

	ret = write(fd, str, ft_strlen(str));
	ft_putchar_fd('\n', fd);
	return (ret + 1);
}
