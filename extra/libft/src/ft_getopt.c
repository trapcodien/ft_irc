/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_getopt.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: garm <garm@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/10/07 02:50:14 by garm              #+#    #+#             */
/*   Updated: 2014/10/24 18:14:11 by garm             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int		opt_dash(char **av, int *ac_pos, int *av_pos, const char *opstring)
{
	(*av_pos)++;
	if (av[*ac_pos][*av_pos] == '-' && av[*ac_pos][(*av_pos) + 1] == '\0')
	{
		(*ac_pos)++;
		(*av_pos) = 0;
		return (OPT_STOP_PARSE);
	}
	return (opt_analyze(av, ac_pos, av_pos, opstring));
}

int		opt_analyze(char **av, int *ac_pos, int *av_pos, const char *opstring)
{
	char		c;
	int			find;

	c = av[*ac_pos][*av_pos];
	find = 0;
	if (av[*ac_pos][*av_pos] == 0)
	{
		(*ac_pos)++;
		(*av_pos) = 0;
		return (OPT_END);
	}
	if (*av_pos == 0 && av[*ac_pos][*av_pos] == '-')
		return (opt_dash(av, ac_pos, av_pos, opstring));
	if (*av_pos != 0 && c != ':' && (find = ft_findc(opstring, c)) != ERROR)
	{
		(*av_pos)++;
		return (c);
	}
	else if (find == ERROR)
	{
		(*av_pos)++;
		return (c * (-1));
	}
	else
		return (OPT_STOP_PARSE);
}

int		opt_is_arg(int op, const char *opstring)
{
	int		find;

	find = ft_findc(opstring, op);
	if (find == ERROR)
		return (0);
	if (opstring[find + 1] == ':')
		return (1);
	return (0);
}

char	*opt_set_arg(int ac, char **av, int *ac_pos, int *av_pos)
{
	int		x;
	int		y;

	if (av[*ac_pos][*av_pos] == '\0')
	{
		(*ac_pos)++;
		*av_pos = 0;
	}
	x = *ac_pos;
	y = *av_pos;
	if (((*ac_pos) == ac) || (av[x][y] &&
							av[x][y] == '-' &&
							av[x][y + 1] &&
							av[x][y + 1] == '-' &&
							av[x][y + 2] == '\0'))
		return (NULL);
	(*ac_pos)++;
	*av_pos = 0;
	return (&(av[x][y]));
}

int		ft_getopt(int ac, char **av, const char *opstring, t_opt *opt)
{
	static int		ac_pos = 1;
	static int		av_pos = 0;
	int				op;

	if (ac < 0)
		return (ft_getopt_control(ac, &ac_pos, &av_pos));
	opt->opt = 0;
	if (ac)
		opt->arg = NULL;
	if (ac == ac_pos)
		return (OPT_STOP_PARSE);
	while ((op = opt_analyze(av, &ac_pos, &av_pos, opstring)) != OPT_STOP_PARSE)
	{
		if (ac == ac_pos)
			return (OPT_STOP_PARSE);
		if (op < 0 && (opt->opt = (op * -1)))
			return (OPT_UNKNOW);
		if (op != OPT_STOP_PARSE && op != OPT_END && op != OPT_UNKNOW)
		{
			if (opt_is_arg(op, opstring))
				opt->arg = opt_set_arg(ac, av, &ac_pos, &av_pos);
			return (op);
		}
	}
	return (OPT_STOP_PARSE);
}
