/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memdup.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: garm <garm@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/06/20 22:04:00 by garm              #+#    #+#             */
/*   Updated: 2014/06/20 22:06:40 by garm             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void		*ft_memdup(const void *ptr, size_t size)
{
	void	*new;

	if (!ptr)
		return (NULL);
	new = ft_memalloc(size);
	if (!new)
		return (NULL);
	ft_memcpy(new, ptr, size);
	return (new);
}
