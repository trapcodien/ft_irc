/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_tabmerge.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: garm <garm@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/12/23 21:30:39 by garm              #+#    #+#             */
/*   Updated: 2014/12/23 23:08:35 by garm             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char		*ft_tabmerge(char **tab, int len)
{
	size_t	size;
	char	*ret;
	int		i;

	if (!tab || !len)
		return (NULL);
	size = len - 1;
	i = 0;
	while (i < len)
	{
		size += ft_strlen(tab[i]);
		i++;
	}
	ret = ft_strnew(size);
	if (!ret)
		return (NULL);
	i = 0;
	while (i < len)
	{
		ft_strcat(ret, tab[i]);
		if (i != len - 1)
			ft_strcat(ret, " ");
		i++;
	}
	return (ret);
}
