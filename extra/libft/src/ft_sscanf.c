/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_sscanf.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: garm <garm@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/12/23 03:54:26 by garm              #+#    #+#             */
/*   Updated: 2015/04/04 00:01:44 by garm             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdarg.h>
#include "libft.h"

static int		ft_find_char(va_list ap, char *str, const char *format, int *i)
{
	char		*chr;
	char		*char_ptr;

	chr = ft_strchr(str, format[i[1] + 2]);
	if (!chr)
	{
		i[2] = 0;
		return (0);
	}
	char_ptr = va_arg(ap, char *);
	*char_ptr = str[i[0]];
	i[1]++;
	i[2]++;
	return (1);
}

static int		ft_find_int(va_list ap, char *str, const char *format, int *i)
{
	char		*chr;
	int			*number;

	chr = ft_strchr(&str[i[0]], format[i[1] + 2]);
	if (!chr)
	{
		i[2] = 0;
		return (0);
	}
	number = va_arg(ap, int *);
	*number = ft_atoi(&str[i[0]]);
	i[0] += ft_strlen(ft_itoa(*number)) - 1;
	i[1]++;
	i[2]++;
	return (1);
}

static int		ft_find_str(va_list ap, char *str, const char *format, int *i)
{
	char		*chr;
	char		*string;

	chr = ft_strchr(&str[i[0]], format[i[1] + 2]);
	if (!chr)
	{
		i[2] = 0;
		return (0);
	}
	string = va_arg(ap, char *);
	ft_strncpy(string, &str[i[0]], chr - &str[i[0]]);
	string[chr - &str[i[0]]] = 0;
	i[0] += chr - &str[i[0]] - 1;
	i[1]++;
	i[2]++;
	return (1);
}

static int		ft_checks(va_list ap, char *str, const char *format, int *i)
{
	if (str[i[0]] == format[i[1]] && format[i[1]] == '%')
		i[1]++;
	else if (format[i[1]] == '%' && format[i[1] + 1] == 'c')
	{
		if (!ft_find_char(ap, str, format, i))
			return (0);
	}
	else if (format[i[1]] == '%' && format[i[1] + 1] == 'i')
	{
		if (!ft_find_int(ap, str, format, i))
			return (0);
	}
	else if (format[i[1]] == '%' && format[i[1] + 1] == 's')
	{
		if (!ft_find_str(ap, str, format, i))
			return (0);
	}
	else if (format[i[1]] != str[i[0]])
	{
		i[2] = 0;
		return (0);
	}
	return (1);
}

int				ft_sscanf(char *str, const char *format, ...)
{
	va_list		ap;
	int			i[3];

	va_start(ap, format);
	i[0] = 0;
	i[1] = 0;
	i[2] = 0;
	while (str[i[0]] && format[i[1]] && ft_checks(ap, str, format, i))
	{
		i[0]++;
		i[1]++;
	}
	va_end(ap);
	if (str[i[0]] || format[i[1]])
		return (-1);
	return (i[2]);
}
