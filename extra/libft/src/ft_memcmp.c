/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memcmp.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: garm <garm@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/06/15 19:57:13 by garm              #+#    #+#             */
/*   Updated: 2014/12/19 15:53:46 by garm             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

int			ft_memcmp(const void *s1, const void *s2, size_t n)
{
	int			ret;
	size_t		i;
	const char	*str1;
	const char	*str2;

	if (!s1 || !s2)
		return (-1);
	str1 = (const char *)s1;
	str2 = (const char *)s2;
	i = 0;
	while (i < n)
	{
		if ((ret = str1[i] - str2[i]) != 0)
			return (ret);
		i++;
	}
	return (0);
}
