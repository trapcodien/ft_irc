# LIBFT #

libft (also called lib42) is a basic C library which contains
basic utils functions like :
ft_putstr, ft_atoi, ft_memalloc, ft_strsplit
(see also [Header files](#markdown-header--header-files))

Summary
=======

[Instructions](#markdown-header--instructions)
==================
* [Download](#markdown-header-download)
* [Compilation](#markdown-header-compilation)
* [First Use](#markdown-header-first-use)

[Memory](#markdown-header--memory)
==================
* [ft_memset](#markdown-header-ft_memset)
* [ft_bzero](#markdown-header-ft_bzero)
* [ft_memalloc](#markdown-header-ft_memalloc)
* [ft_memdel](#markdown-header-ft_memdel)
* [ft_memcpy](#markdown-header-ft_memcpy)
* [ft_memccpy](#markdown-header-ft_memccpy)
* [ft_memmove](#markdown-header-ft_memmove)
* [ft_memchr](#markdown-header-ft_memchr)
* [ft_memcmp](#markdown-header-ft_memcmp)
* [ft_memdup](#markdown-header-ft_memdup)

[Strings](#markdown-header--strings)
===================
* [ft_strnew](#markdown-header-ft_strnew)
* [ft_strdel](#markdown-header-ft_strdel)
* [ft_strclr](#markdown-header-ft_strclr)
* [ft_strlen](#markdown-header-ft_strlen)
* [ft_strlenc](#markdown-header-ft_strlenc)
* [ft_strdup](#markdown-header-ft_strdup)
* [ft_strcpy](#markdown-header-ft_strcpy)
* [ft_strncpy](#markdown-header-ft_strncpy)
* [ft_strcat](#markdown-header-ft_strcat)
* [ft_strncat](#markdown-header-ft_strncat)
* [ft_strchr](#markdown-header-ft_strchr)
* [ft_strrchr](#markdown-header-ft_strrchr)
* [ft_strstr](#markdown-header-ft_strstr)
* [ft_strnstr](#markdown-header-ft_strnstr)
* [ft_strcmp](#markdown-header-ft_strcmp)
* [ft_strncmp](#markdown-header-ft_strncmp)
* [ft_strequ](#markdown-header-ft_strequ)
* [ft_strnequ](#markdown-header-ft_strnequ)
* [ft_strsub](#markdown-header-ft_strsub)
* [ft_strjoin](#markdown-header-ft_strjoin)
* [ft_strtrim](#markdown-header-ft_strtrim)
* [ft_strtrimc](#markdown-header-ft_strtrimc)
* [ft_findc](#markdown-header-ft_findc)
* [ft_strcountc](#markdown-header-ft_strcountc)
* [ft_strdelmultic](#markdown-header-ft_strdelmultic)
* [ft_strisnum](#markdown-header-ft_strisnum)
* [ft_strsplit](#markdown-header-ft_strsplit)
* [ft_strsplit_strict](#markdown-header-ft_strsplit_strict)
* [ft_splitdel](#markdown-header-ft_splitdel)

[Digits, numbers and characters](#markdown-header--digits-numbers-and-characters)
============
* [ft_atoi](#markdown-header-ft_atoi)
* [ft_itoa](#markdown-header-ft_itoa)
* [ft_isascii](#markdown-header-ft_isascii)
* [ft_isprint](#markdown-header-ft_isprint)
* [ft_isupper](#markdown-header-ft_isupper)
* [ft_islower](#markdown-header-ft_islower)
* [ft_isalpha](#markdown-header-ft_isalpha)
* [ft_isdigit](#markdown-header-ft_isdigit)
* [ft_isalnum](#markdown-header-ft_isalnum)
* [ft_iswhite](#markdown-header-ft_iswhite)
* [ft_toupper](#markdown-header-ft_toupper)
* [ft_tolower](#markdown-header-ft_tolower)
* [ft_ban_neg](#markdown-header-ft_ban_neg)

[Print](#markdown-header--print)
============
* [ft_putchar](#markdown-header-ft_putchar)
* [ft_putstr](#markdown-header-ft_putstr)
* [ft_putendl](#markdown-header-ft_putendl)
* [ft_putnbr](#markdown-header-ft_putnbr)
* [ft_putnstr](#markdown-header-ft_putnstr)
* [ft_printf](#markdown-header-ft_printf)
* [ft_putchar_fd](#markdown-header-ft_putchar_fd)
* [ft_putstr_fd](#markdown-header-ft_putstr_fd)
* [ft_putendl_fd](#markdown-header-ft_putendl_fd)
* [ft_putnbr_fd](#markdown-header-ft_putnbr_fd)
* [ft_putnstr_fd](#markdown-header-ft_putnstr_fd)
* [ft_fprintf](#markdown-header-ft_fprintf)
* [ft_error](#markdown-header-ft_error)
* [ft_fatal](#markdown-header-ft_fatal)

[Input](#markdown-header--input)
============
* [get_next_line](#markdown-header-get_next_line)

[Header Files](#markdown-header--header-files)
============
* [libft.h](#markdown-header-libft-header-file)
* [libft_config.h](#markdown-header-libft_config-header-file)
* [get_next_line.h](#markdown-header-get_next_line-header-file)



#**       - Instructions          **#
## Download ##
```
#!shell
git clone https://trapcodien@bitbucket.org/trapcodien/libftv2.git libft
```

## Compilation ##
```
#!shell
make -C libft
gcc main.c -L ./libft -I ./libft/includes -lft
```

## First Use ##
```
#!c
#include "libft"

int		main(void)
{
	ft_putendl("libft successfully loaded.");
	return (0);
}
```



#**         - Memory            **#
## ft_memset ##
```
#!c
void 		*ft_memset(void *b, int c, size_t len);
```
**ft_memset()** write **len** bytes of **c** value to **b**.

**return** : **ft_memset()** return *b*

## ft_bzero ##
```
#!c
void		ft_bzero(void *s, size_t n);
```
**ft_bzero()** set **n** bytes to zero from **s**.
if **s** is NULL, **ft_bzero()** does nothing.

## ft_memalloc ##
```
#!c
void		*ft_memalloc(size_t size);
```
**ft_memalloc()** dynamically allocates **size** bytes (with **malloc(3)**),
and **ft_bzero** this area.

**return** : pointer to **allocated memory** is returned. if allocation fails,
**NULL** is returned.

## ft_memdel ##
```
#!c
void		ft_memdel(void **ap)
```
**ft_memdel()** call **free(3)** on **\*ap** only if **\*ap** is not null

## ft_memcpy ##
```
#!c
void		*ft_memcpy(void *s1, const void *s2, size_t n);
```
The **ft_memcpy()** function copies **n** bytes from memory area **s2**
to memory area **s1**.

If **s1** and **s2** overlap, behavior is undefined.
Applications in which **s1** and **s2** might overlap
should use ft_memmove instead.

**return** : the original value of **s1** is returned.

## ft_memccpy ##
```
#!c
void		*ft_memccpy(void *s1, const voidd *s2, int c, size_t n);
```
The **ft_memccpy()** function copies bytes from string **s2** to string **s1**.
If the character **c** occurs in the string **s2**, the copy stops and
a pointer to the **byte after the copy of c** in the string s1 is returned.
Otherwise, **n** bytes are copied, and a **NULL** pointer is returned.

The source and destination strings should not overlap,
as the behavior is undefined.


## ft_memmove ##
```
#!c
void		*ft_memmove(void *s1, const void *s2, size_t n);
```
**ft_memmove()** function copies **n** bytes from **s2** to **s1**.
**s1** and **s2** may overlap, copy is done in a non-destructive manner.

**return** : ft_memmove() return **s1**

## ft_memchr ##
```
#!c
void		*ft_memchr(void *s, int c size_t n);
```
**ft_memchr()** function locates the first occurence of **c** in string **s**.

**return** : pointer to the byte located, or **NULL** if no such bytes exists
within **n** bytes.

## ft_memcmp ##
```
#!c
int			ft_memcmp(const void *s1, const void *s2, size_t n);
```
**ft_memcmp()** function compares byte string **s1** against byte string **s2**.
Both strings are assumed to be **n** bytes long.

**return** : **ft_memcmp()** function returns zero if the two strings are
identical, otherwise returns the difference between the first two 
differing bytes. Zero-length strings are always identical.

## ft_memdup ##
```
#!c
void		*ft_memdup(const void *ptr, size_t size);
```
**ft_memdup()** function dynamically allocates **size** bytes, and
copy all of the **size** bytes from **ptr** to the **new allocated memory**

**return** : **new allocated memory** or **NULL** if allocation fails.


#**           - Strings             **#
## ft_strnew ##
```
#!c
char		*ft_strnew(size_t size);
```
**ft_strnew()** function call
[**ft_memalloc(**size** + 1)**](#markdown-header-ft_memalloc)

**return** : **new allocated string** (converted as **char \***) or **NULL**
if allocation fails.

## ft_strdel ##
```
#!c
void		ft_strdel(char **s);
```
**ft_strdel()** functuin call
[**ft_memdel()**](#markdown-header-ft_memdel) on null terminated string **s**.

## ft_strclr ##
```
#!c
void		ft_strclr(char *s)
```
**ft_strclr()** function call
[**ft_bzero()**](#markdown-header-ft_bzero) like this :
```
#!c
/* ft_strclr.c Source code */
#include "libft.h"

void		ft_strclr(char *s)nstr{
	ft_bzero((void *s)s, ft_strlen(s));
}
```

## ft_strlen ##
```
#!c
size_t		ft_strlen(const char *s);
```
**ft_strlen()** function counts number of bytes in null-terminated string **s**.

**return** : return **length** of the string **s** or 0 if s is **NULL**.

## ft_strlenc ##
```
#!c
size_t		ft_strlenc(const char *s, char c);
```
**ft_strlenc()** function counts number of bytes in null-terminated
string **s**.
if character **c** is found, **ft_strlenc** stop to count and return **length**.

**return** : **s** to **c** length,
or full length of **s** if **c** is not found, or 0 is **s** is **NULL**;


## ft_strdup ##
```
#!c
char		*ft_strdup(const char *s);
```
**ft_strdup()** function dynamically allocates memory and copy the **s**
null-terminated string to it.

**return** : new allocated string or NULL if allocation fails.

## ft_strcpy ##
```
#!c
char		*ft_strcpy(char *s1, const char *s2);
```
**ft_strcpy()** function copy all bytes from null-terminated string **s2** to
the null-terminated string **s1**.

**s1** and **s2** should not overlap.

**Warning** : Overflow is your responsability.

**return** : **s1** is returned.

## ft_strncpy ##
```
#!c
char		*ft_strncpy(char *s1, const char *s2, size_t n);
```
**ft_strncpy()** function copy **n** bytes from null-terminated string **s2**
to the null-terminated string **s1**.
**s1** and **s2** should not overlap.
Warning : Overflow is your responsability.

**return** : **s1** is returned.

## ft_strcat ##
```
#!c
char		*ft_strcat(char *s1, const char *s2);
```
**ft_strcat()** function append a copy of the null-terminated string **s2** to
the end of null-terminated string **s1**. and add null byte to the end.
The string **s1** must have sufficient space to hold the result.

**return** : **s1** is returned.

## ft_strncat ##
```
#!c
char		*ft_strncat(char *s1, const char *s2, size_t n);
```
**ft_strncat()** function copy **n** bytes from **s2** to **s1** and add
null bytei to the end.
The string **s1** must have sufficient space to hold the result.

**return** : **s1** is returned.

## ft_strchr ##
```
#!c
char		*ft_strchr(const char *s, int c);
```
**ft_strchr()** function locates the first occurence of **c** in the string
pointed by **s**. The null-terminated character is considered to be part of the
string; therefore if **c** is '\0', the function locates the terminating '\0'.

**return** : return a pointer to the located character, or NULL if the character
does not appear in the string.

## ft_strrchr ##
```
#!c
char		*ft_strrchr(const char *s, int c);
```
**ft_strrchr()** function is identical to
[**ft_strchr()**](#markdown-header-ft_strchr), expect it locates the last
occurence of **c**.

**return** : return a pointer to the located character, or NULL if the character
does not appear in the string.

## ft_strstr ##
```
#!c
char		*ft_strstr(const char *s1, const char *s2);
```
**ft_strstr()** function locates the first occurence of the null-terminated
string **s2** in the null-terminaetd string **s1**.

**return** : if **s2** is an empty string or is **NULL**, **s1** is returned.
if **s2** occurs nowhere in **s1**, **NULL** is returned.
otherwise, a pointer to the first character of the first occurence of
**s1** is returned.

## ft_strnstr ##
```
#!c
char		*ft_strnstr(const char *s1, const char *s2, size_t n);
```

**ft_strnstr()** function locates the first occurence of the null-terminated
string **s2** in the string **s1**, where not more **n** bytes are searched.
Characters that appear after a '\0' character are not searched.

**return** : if **s2** is an empty string or is **NULL**, **s1** is returned.
if **s2** occurs nowhere in **s1**, **NULL** is returned.
otherwise, a pointer to the first character of the first occurence of
**s1** is returned.

## ft_strcmp ##
```
#!c
int			ft_strcmp(const char *s1, const char *s2)
```
**ft_strcmp()** function lexicographically compare the null-terminated
strings **s1** and **s2**.

**return** : **ft_strcmp()** function return an integer greater than, equal to,
or less than 0, according as the string **s1** is greater than, equal to,
or less than the string **s2**.

## ft_strncmp ##
```
#!c
int			ft_strncmp(const char *s1, const char *s2, size_t n);
```
**ft_strncmp()** function lexicographically compare the null-terminated
strings **s1** and **s2**, but not more than **n** bytes.

**return** : **ft_strncmp()** function return an integer greater than, equal to,
or less than 0, according as the string **s1** is greater than, equal to,
or less than the string **s2**.

## ft_strequ ##
```
#!c
int			ft_strequ(const char *s1, const char *s2);
```
**ft_strequ()** function compares **s1** and **s2** strings.

**return** : if string **s1** is equal to string **s2**, 1 is returned.
otherwise, 0 is returned.

## ft_strnequ ##
```
#!c
int			ft_strnequ(const char *s1, const char *s2);
```
**ft_strnequ()** function compares **s1** and **s2** strings.

**return** : if string **s1** is not equal to string **s2**, 1 is returned.
otherwise, 0 is returned.

## ft_strsub ##
```
#!c
char		*ft_strsub(const char *s, unsigned int start, size_t len);
```
**ft_strsub()** function make a fresh copy of **s** started at **start** and
finished in **len**.

**return** : cutted fresh string is returned.
if allocation fails, **NULL** is returned.

## ft_strjoin ##
```
#!c
char		*ft_strjoin(const char *s1, const char *s2);
```

**ft_strjoin()** function is the result of the concatenation of **s1**
and **s2** in a new fresh null-terminated string.

**return** : concatened new fresh string, **NULL** if allocation fails.

## ft_strtrim ##

```
#!c
char		*ft_strtrim(const char *s);
```
**ft_strtrim()** function makes a fresh copy of **s** without **whitespaces**
at the beggining and end.

**whitespaces** is considered to be : ' ', '\n' and '\t'

**return** : new fresh trimmed string is returned.

## ft_strtrimc ##
```
#!c
char		*ft_strtrimc(const char *s, char c);
```
**ft_strtrimc()** function makes a fresh copy of **s** without **c**
at the beggining and end.

**return** : new fresh trimmed string is returned.

## ft_findc ##
```
#!c
int			ft_findc(const char *s, char c);
```
**ft_findc()** function search byte **c** in null-terminated string **s**.

**return** : if **c** is found, 1 is returned. otherwise, 0 is returned.

## ft_strcountc ##
```
#!c
int			ft_strcountc(const char *s, char c);
```
**ft_strcountc()** function counts the number of occurences of **c** in **s**
string.

**return** : number of occurences of **b** in **s**, or 0 if nothing
is found or if **s** is **NULL**

## ft_strdelmultic ##
```
#!c
char		*ft_strdelmultic(const char *s, char c);
```
**ft_strdelmultic()** function makes a fresh copy of string **s** taking care
to remove all double **c** characters.

**return** : fresh copy, without double **c** characters. if allocation fails,
**NULL** is returned.

## ft_strisnum ##
```
#!c
int			ft_strisnum(const char *str);
```
**ft_strisnum()** function check if string **str** can be converted as
signed integer (with ft_atoi).

**return** : 1 if **str** is a number. else 0.

## ft_strsplit ##
```
#!c
char		**ft_strsplit(const char *s, char c);
```
**ft_strsplit()** function is a
[php explode](http://www.php.net/manual/en/function.explode.php) remake.
It makes a strings array from **s** string and use **c** character as
separator.

**return** : null-terminated strings array.

**Example** :
```
#!c
#include "libft.h"

int		main(void)
{
	char	**split;

	split = ft_strsplit("   Hello    World     !    ", ' ');
	/* split => {"Hello", "World", "!", NULL} */
	ft_putendl(split[0]);
	ft_putendl(split[1]);
	ft_putendl(split[2]);
	ft_splitdel(&split);
	return (0);
}
```

## ft_strsplit_strict ##
```
#!c
char		*ft_strsplit_strict(const char *s, char c);
```
**ft_strsplit_strict()** function is same as
[ft_strsplit()](#markdown-header-ft_strsplit) function expect that does not
trim and remove double separator character **c**.

**return** : null-terminated strings array.

**Example** :
```
#!c
#include "libft.h"

int		main(void)
{
	char	**split;

	split = ft_strsplit_strict("Hello,,World,", ',');
	/* split => {"Hello", "", "World", "", NULL} */
	ft_putendl(split[0]);
	ft_putendl(split[1]);
	ft_putendl(split[2]);
	ft_putendl(split[3]);
	ft_splitdel(&split);
	return (0);
}
```

## ft_splitdel ##
```
#!c
void		ft_splitdel(char ***split);
```

**ft_splitdel()** function free a null-terminated strings array.
if **\*split** or **split** is NULL, nothing is free.

**See also** : [ft_strsplit()](#markdown-header-ft_strsplit)




#**           - Digits, numbers and characters             **#


## ft_atoi ##
```
#!c
int			ft_atoi(const char *str);
```
**ft_atoi()** function convert string **str** as a signed integer value.

**return** : converted signed integer value is returned,
if **str** is NULL or nor a number, 0 is returned.

## ft_itoa ##
```
#!c
char		*ft_itoa(const int n);
```
**ft_itoa()** function convert signed integer **n** as a string.

**return** : returned string is **static**, please never free this ptr.

Note : if **ft_itoa()** returned value must be stored,
please use [ft_strdup()](#markdown-header-ft_strdup) for save value.

## ft_isascii ##
```
#!c
int			ft_isascii(int c);
```
**ft_isascii()** function verify if **c** is an **ascii character**.

**ascii characters** are any value between 0 and 127 included.
(decimal ascii table)

**return** : 1 if **c** is ascii, otherwise, 0 is returned.

## ft_isprint ##
```
#!c
int			ft_isprint(int c);
```
**ft_isprint()** function verify if **c** is a **printable character**.

**printable characters** are any value (including space) between 32 and 126
included. (decimal ascii table)

**return** : 1 if **c** is printable, otherwise, 0 is returned.

## ft_isupper ##
```
#!c
int			ft_isupper(int c);
```
**ft_isupper()** function verify if **c** is an **upper-case letter**.

**upper-case letters** are any value between 65 and 90 included.
(decimal ascii table)

**return** : 1 if **c** is upper-case letter, otherwise, 0 is returned.

## ft_islower ##
```
#!c
int			ft_islower(int c);
```
**ft_islower()** function verify if **c** is a **lower-case letter**.

**lower-case-letters** are any value between 97 and 122 included.
(decimal ascii table)

**return** : 1 if **c** is lower-case letter, otherwise, 0 is returned.

## ft_isalpha ##
```
#!c
int			ft_isalpha(int c);
```
**ft_isalpha()** function verify if **c** is an **upper-case letter** or
is a **lower-case letter**.

**return** : 1 if **c** is an alphabetical value. otherwise, 0 is returned.

**See also** : [ft_isupper()](#markdown-header-ft_isupper)
and [ft_islower()](#markdown-header-ft_islower) functions for more details.

## ft_isdigit ##
```
#!c
int			ft_isdigit(int c);
```
**ft_isdigit()** function verifiy if **c** is a **digit** value.

**digits** are any value between 48 and 57 included.
(decimal ascii table)

**return** : 1 if **c** is digit, otherwise, 0 is returned.


## ft_isalnum ##
```
#!c
int			ft_isalnum(int c);
```

**ft_isalnum()** function verify if **c** is an alphanumerical value.

**alphanumerical** value is a value which can be digit or letter.

**return** : 1 if **c** is an alphanumerical value. otherwise, 0 is returned.

**See also** : [ft_isalpha()](#markdown-header-ft_isalpha)
and [ft_isdigit()](#markdown-header-ft_isdigit) functions for more details.

## ft_iswhite ##
```
#!c
int			ft_iswhite(int c);
```
**ft_iswhite()** function verify if **c** is a whitespace value.

**whitespace** value is a value which can be ' ', '\t' or '\n'.

**return** : 1 if **c** is a whitespace value. otherwise, 0 is returned.

## ft_toupper ##
```
#!c
int			ft_toupper(int c);
```
**ft_toupper()** function convert **c** to an upper-case letter.

**return** : if **c** is not a lower-case letter, original **c**
value is returned.

## ft_tolower ##
```
#!c
int			ft_tolower(int c);
```
**ft_tolower()** function convert **c** to a lower-case letter.

**return** : if **c** is not an upper-case letter, original **c**
value is returned.

## ft_ban_neg ##
```
#!c
int			ft_ban_neg(int n);
```
**ft_ban_neg()** function was designed for ban negative numbers.

**return** : if **n** is negative. 0 is returned, otherwise **n** is returned.



#**           - Print             **#

## ft_putchar ##
```
#!c
int			ft_putchar(const char c);
```
**ft_putchar()** function prints character **c** on standard output.

**return** : number of bytes written (in this case, this always will be 1).

## ft_putstr ##
```
#!c
int			ft_putstr(const char *str);
```
**ft_putstr()** function prints null-terminated string **str** on standard
output.

**return** : number of bytes written.

## ft_putendl ##
```
#!c
int			ft_putendl(const char *str);
```
**ft_putendl()** function prints null-terminated string **str** on standard
output and print an end of line character '\n'.

**return** : number of bytes written.

## ft_putnbr ##
```
#!c
void		ft_putnbr(const int n)
```
**ft_putnbr()** function prints **n** signed integer value as a null-terminated
string on standard output.

## ft_putnstr ##
```
#!c
char		*ft_putnstr(const char *str, size_t n)
```
**ft_putnstr()** function prints **n** bytes of null-terminated string **str**
on the standard output.

if null byte is found, printing is stopped.

**return** : a pointer to the next of the string.

(if **n** >= ft_strlen(str),
a pointer to the terminate null byte of **str** is returned.)

## ft_printf ##
```
#!c
int			ft_printf(const char *format, ...);
```
**ft_printf()** function is a dummy implementation of printf(3).

**formatted string** can juste use **%s**, **%c**, **%i** and **%d** flags.

**%i** is equal to **%d** flag.

**return** : number of bytes written.

**Example** :
```
#!c
#include "libft.h"

int		main(void)
{
	ft_printf("Team %c : I'm %s and I'm %i years old.", 'a', "trapcodien", 42);
	return (0);
}
```

## ft_putchar_fd ##
```
#!c
int			ft_putchar_fd(const char c, int fd);
```
**ft_putchar_fd()** function prints character **c** on given file descriptor
**fd**.

**return** : number of bytes written (in this case, this always will be 1).

## ft_putstr_fd ##
```
#!c
int			ft_putstr_fd(const char *str, int fd);
```
**ft_putstr_fd()** function prints null-terminated string **str** on given
file descriptor **fd**.

**return** : number of bytes written.

## ft_putendl_fd ##
```
#!c
int			ft_putendl_fd(const char *str, int fd);
```
**ft_putendl_fd()** function prints null-terminated string **str** on given
file descriptor **fd** and print an end of line character '\n'.

**return** : number of bytes written.

## ft_putnbr_fd ##
```
#!c
void		ft_putnbr_fd(const int n, int fd);
```
**ft_putnbr()_fd** function prints **n** signed integer value
as a null-terminated string on given file descriptor **fd**.

## ft_putnstr_fd ##
```
#!c
char		*ft_putnstr_fd(const char *str, size_t n, int fd);
```
**ft_putnstr_fd()** function prints **n** bytes
of null-terminated string **str** on given file descriptor **fd**.

if null byte is found, printing is stopped.

**return** : a pointer to the next of the string.

(if **n** >= ft_strlen(str),
a pointer to the terminate null byte of **str** is returned.)

## ft_fprintf ##
```
#!c
int			ft_fprintf(int fd, const char *format, ...);
```
**ft_fprintf()** function is same as [ft_printf()](#markdown-header-ft_printf)
function but prints **formatted string** on the given file descriptor **fd**.

**return** : number of bytes written.

## ft_error ##
```
#!c
int			ft_error(const char *str);
```
**ft_error()** function prints null-terminated string **str** on standard
error output. (**STDERR_FILENO**)

**return** : 1 is returned.

**Example** :
```
#!c
#include "libft.h"

int		main(void)
{
	int		error;

	error = 1;
	if (error)
		return (ft_error("It's an error message.\n"));
	return (0);
}
```

## ft_fatal ##
```
#!c
void		ft_fatal(const char *str);
```
**ft_fatal()** function prints null-terminated string **str** on standard
error output and call **exit()** function with error code 1.



#**           - Input             **#

## get_next_line ##
```
#!c
int			get_next_line(int fd, char **line);
```
**get_next_line()** function read given file descriptor **fd** and place
readed line (line must be finished by '\n') in **\*line**.

**return** : if **get_next_line()** function fail, -1 is returned.

**get_next_line()** function returns 1 when line is successfully readed, or 0
when last line is readed.




#**           - Header Files             **#

## libft header file ##
```
#!c
/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   libft.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: garm <garm@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/06/15 17:10:04 by garm              #+#    #+#             */
/*   Updated: 2014/06/21 20:00:08 by garm             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LIBFT_H
# define LIBFT_H

# include <unistd.h>
# include <sys/types.h>
# include "libft_config.h"
# include "get_next_line.h"

/*
** Memory
*/
void		*ft_memset(void *b, int c, size_t len);
void		ft_bzero(void *s, size_t n);
void		*ft_memalloc(size_t size);
void		ft_memdel(void **ap);
void		*ft_memcpy(void *s1, const void *s2, size_t n);
void		*ft_memccpy(void *s1, const void *s2, int c, size_t n);
void		*ft_memmove(void *s1, const void *s2, size_t n);
void		*ft_memchr(const void *s, int c, size_t n);
int			ft_memcmp(const void *s1, const void *s2, size_t n);
void		*ft_memdup(const void *ptr, size_t size);

/*
** Strings
*/
char		*ft_strnew(size_t size);
void		ft_strdel(char **s);
void		ft_strclr(char *s);
size_t		ft_strlen(const char *s);
size_t		ft_strlenc(const char *s, char c);
char		*ft_strdup(const char *s);
char		*ft_strcpy(char *s1, const char *s2);
char		*ft_strncpy(char *s1, const char *s2, size_t n);
char		*ft_strcat(char *s1, const char *s2);
char		*ft_strncat(char *s1, const char *s2, size_t n);
char		*ft_strchr(const char *s, int c);
char		*ft_strrchr(const char *s, int c);
char		*ft_strstr(const char *s1, const char *s2);
char		*ft_strnstr(const char *s1, const char *s2, size_t n);
int			ft_strcmp(const char *s1, const char *s2);
int			ft_strncmp(const char *s1, const char *s2, size_t n);
int			ft_strequ(const char *s1, const char *s2);
int			ft_strnequ(const char *s1, const char *s2);
char		*ft_strsub(const char *s, unsigned int start, size_t len);
char		*ft_strjoin(const char *s1, const char *s2);
char		*ft_strtrim(const char *s);
char		*ft_strtrimc(const char *s, char c);
int			ft_findc(const char *s, char c);
int			ft_strcountc(const char *s, char c);
char		*ft_strdelmultic(const char *s, char c);
char		**ft_strsplit(const char *s, char c);
void		ft_splitdel(char ***split);

/*
** Digits, Numbers and characters
*/
int			ft_atoi(const char *str);
char		*ft_itoa(const int n);
int			ft_isascii(int c);
int			ft_isprint(int c);
int			ft_isupper(int c);
int			ft_islower(int c);
int			ft_isalpha(int c);
int			ft_isdigit(int c);
int			ft_isalnum(int c);
int			ft_iswhite(int c);
int			ft_toupper(int c);
int			ft_tolower(int c);
int			ft_ban_neg(int n);

/*
** Print
*/
int			ft_putchar(const char c);
int			ft_putstr(const char *str);
int			ft_putendl(const char *str);
void		ft_putnbr(const int n);
char		*ft_putnstr(const char *str, size_t n);
int			ft_printf(const char *format, ...);

int			ft_putchar_fd(const char c, int fd);
int			ft_putstr_fd(const char *str, int fd);
int			ft_putendl_fd(const char *str, int fd);
void		ft_putnbr_fd(const int n, int fd);
char		*ft_putnstr_fd(const char *str, size_t n, int fd);
int			ft_fprintf(int fd, const char *format, ...);

int			ft_error(const char *str);
void		ft_fatal(const char *str);

#endif
```

## libft_config header file ##
```
#!c
/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   libft_config.h                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: garm <garm@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/06/17 00:47:42 by garm              #+#    #+#             */
/*   Updated: 2014/06/17 00:48:31 by garm             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LIBFT_CONFIG_H
# define LIBFT_CONFIG_H

# define ERROR -1
# define BUFF_SIZE 1024

#endif
```

## get_next_line header file ##
```
#!c
/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.h                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: garm <garm@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/06/17 00:54:10 by garm              #+#    #+#             */
/*   Updated: 2014/06/17 00:57:47 by garm             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef GET_NEXT_LINE_H
# define GET_NEXT_LINE_H

# define BUF		BUFF_SIZE
# define CLEAN		-1
# define THIS_CRAP	NULL

typedef struct		s_gnl
{
	int				fd;
	char			*memory;
	struct s_gnl	*next;
}					t_gnl;

int					get_next_line(const int fd, char **line);

#endif
```

