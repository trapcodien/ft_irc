/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   common.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: garm <garm@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/04/01 15:37:18 by garm              #+#    #+#             */
/*   Updated: 2015/11/19 16:05:27 by garm             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef COMMON_H
# define COMMON_H

# include "libft.h"
# include "libftdata.h"
# include "libftsock.h"

# define EVENT_RESIZE 410
# define KEY_ESC 27
# define KEY_RETURN 127
# define KEY_DELETE 330
# define KEY_TAB 9
# define NOTHING -1

# define CMD_UNKNOW 0
# define CMD_NICK 1
# define CMD_JOIN 2
# define CMD_LEAVE 3
# define CMD_MSG 4
# define CMD_WHO 5
# define CMD_EXIT 6
# define CMD_HELP 7

# define USER_MAX_LENGTH 9

#endif
