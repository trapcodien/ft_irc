/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   client.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: garm <garm@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/04/01 15:34:09 by garm              #+#    #+#             */
/*   Updated: 2015/12/01 08:24:33 by garm             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef CLIENT_H
# define CLIENT_H

# include <ncurses.h>
# include "common.h"

# define LINE_SIZE (4096 - 1)

# define COLUMNS_ALIGN 4

# define SERV_GREEN_COLOR 1
# define SERV_COLOR 2
# define DEFAULT_COLOR 3
# define CLIENT_COLOR 4
# define PRIVATE_TITLE_COLOR 5
# define PRIVATE_MSG_COLOR 6
# define ME_COLOR 7

# define F_ISNOW "S:%s:%s is now %s\n"
# define GCBN(gui, chan) (get_chan_by_name(gui, chan))

# define COLOR_ON(p, win) wattron(win, COLOR_PAIR(p))
# define COLOR_OFF(p, win) wattroff(win, COLOR_PAIR(p))

# define DEFAULT_PORT 1337

struct s_remote;

typedef struct		s_line
{
	char			buf[LINE_SIZE + 1];
	char			*buf_end;
	char			*cursor;
	char			*start;
	char			*end;
}					t_line;

typedef struct		s_position
{
	short			cols;
	short			lines;
}					t_position;

typedef struct		s_colors
{
	short			title;
	short			content;
}					t_colors;

typedef struct		s_msg
{
	t_elsd			priv;
	char			*name;
	char			*content;
	size_t			length;
	t_colors		colors;
}					t_msg;

typedef struct		s_chan
{
	struct s_chan	*next;
	struct s_chan	*prev;
	char			*name;
	t_lsd			*messages;
}					t_chan;

typedef struct		s_cursor
{
	t_chan			*chan;
	t_msg			*msg;
	size_t			pos;
	t_bool			at_end;
	t_bool			gui_is_full;
}					t_cursor;

typedef struct		s_gui
{
	struct s_remote	*remote;
	WINDOW			*prompt;
	WINDOW			*chatbox;
	t_chan			*empty_chan;
	t_line			*line;
	t_cursor		cursor;
	t_lsd			*channels;
	char			nickname[USER_MAX_LENGTH + 1];
}					t_gui;

typedef struct		s_remote
{
	t_net			priv;
	t_gui			*gui;
}					t_remote;

typedef struct		s_res
{
	char			chan[BUFF_SIZE + 1];
	char			old_name[USER_MAX_LENGTH];
	char			name[USER_MAX_LENGTH + 1];
	char			msg[BUFF_SIZE + 1];
}					t_res;

/*
** client.c
*/
void				connect_server(t_gui *g, t_net *net, char *host, int port);

/*
** client_gui.c
*/
void				gui_init(t_remote *remote);
void				gui_end(t_gui *gui);
void				gui_refresh(t_gui *gui);

/*
** client_prompt.c
*/
WINDOW				*create_prompt(void);
void				destroy_prompt(WINDOW *win);
WINDOW				*clear_prompt(WINDOW *win);
void				prompt_refresh(t_gui *gui);

/*
** client_chatbox.c
*/
WINDOW				*create_chatbox(void);
void				destroy_chatbox(WINDOW *win);
WINDOW				*clear_chatbox(WINDOW *win);

/*
** client_chatbox_move.c
*/
void				chatbox_up(t_gui *gui, t_lsd *messages, t_cursor *c);
void				chatbox_down(t_gui *gui, t_lsd *messages, t_cursor *c);
void				chatbox_end(t_gui *gui);

/*
** client_line.c
*/
t_line				*line_init(t_line *line);
t_bool				line_resize(t_line *line);
void				line_draw(t_line *line, WINDOW *win);
t_bool				line_addch(t_line *line, char ch);

/*
** client_keys.c
*/
t_bool				line_left(t_line *line);
void				line_right(t_line *line);
void				line_delete(t_line *line);
void				line_backdelete(t_line *line);

/*
** client_parser.c
*/
void				parse_res(t_gui *gui, char *buffer, t_res *res);

/*
** client_parser_extra.c
*/
void				client_joined(t_gui *gui, t_res *res);
void				client_leaved(t_gui *gui, t_res *res);
void				client_join(t_gui *gui, t_res *res);
void				client_leave(t_gui *gui, t_res *res);
void				client_message(t_gui *gui, t_res *res);

/*
** client_show.c
*/
t_msg				*show_msg(t_gui *gui, char *msg, t_chan *c);
t_msg				*show_who_msg(t_gui *gui, char *name, t_chan *c);
t_msg				*show_pm(t_gui *gui, t_chan *c, char *name, char *msg);
t_msg				*show_client_msg(t_gui *gui, t_res *res, t_chan *c);
t_msg				*show_leave_msg(t_gui *gui, t_res *res, t_chan *c);

/*
** client_msg.c
*/
t_msg				*create_msg(char *name, char *content);
void				destroy_msg(t_msg *m);
void				destroy_messages_list(t_lsd *l);

/*
** client_chan.c
*/
t_chan				*create_chan(char *name);
void				destroy_chan(t_chan *c);
void				destroy_channels_list(t_lsd *channels);

/*
** client_print.c
*/
void				chatbox_print(WINDOW *chat, t_lsd *messages, t_cursor *c);

/*
** client_utils.c
*/
void				channel_switch(t_gui *gui);
void				gui_process_key(t_gui *gui, int key, char *c);
t_chan				*get_chan_by_name(t_gui *gui, char *name);

/*
** client_send.c
*/
char				*extract_line(t_gui *gui, size_t *size_ptr);
int					parse_unconnected_cmd(t_gui *gui, char *cmd);
void				send_msg(t_gui *gui);

/*
** client_event.c
*/
void				event_stdin(t_remote *net, int key);
size_t				event_recv(t_com *server, t_cbuf *cbuf, size_t size);
void				event_ack(t_com *com, void *data, size_t size);
void				event_disconnect(t_com *com);

#endif
