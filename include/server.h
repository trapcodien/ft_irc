/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   server.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: garm <garm@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/04/01 15:34:09 by garm              #+#    #+#             */
/*   Updated: 2015/12/01 06:24:31 by garm             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef SERVER_H
# define SERVER_H

# include "common.h"

typedef struct			s_chan
{
	struct s_chan		*next;
	char				*name;
}						t_chan;

typedef struct			s_client
{
	t_com				priv;
	char				nick[10];
	t_chan				*current_chan;
}						t_client;

typedef struct			s_cmd
{
	char				type;
	char				*str1;
	char				*str2;
}						t_cmd;

typedef struct			s_connected
{
	struct s_connected	*next;
	t_client			*client;
	t_chan				*channel;
}						t_connected;

typedef struct			s_irc
{
	t_port				priv;
	t_chan				*channels;
	t_connected			*connected;
	t_bool				verbose;
}						t_irc;

/*
** server_parser.c
*/
t_cmd					extract_cmd(t_cbuff *cbuf, size_t size);

/*
** server_execute.c
*/
void					execute_cmd(t_client *c, t_cmd cmd);

/*
** server_channel.c
*/
t_chan					*create_channel(t_irc *server, char *chan_name);
void					destroy_channel(t_irc *server, t_chan *channel);
void					destroy_all_channels(t_irc *server);

/*
** server_nick.c
*/
void					cmd_nick(t_client *c, char *nickname);

/*
** server_join.c
*/
void					cmd_join(t_client *c, char *chan_name);
void					irc_join_msg(t_client *c, t_chan *chan, t_client *who);

/*
** server_leave.c
*/
void					cmd_leave(t_client *c, char *chan_name);
void					leave_channel(t_client *c, t_chan *channel);
void					irc_leave_msg(t_client *c, t_chan *chan, t_client *who);

/*
** server_msg.c
*/
void					irc_msg(t_client *c, char *msg);
void					irc_join_broadcast(t_client *c);
void					cmd_msg(t_client *c, char *target, char *msg);
void					irc_join_confirmation(t_client *c, char *chan_name);
void					irc_leave_confirmation(t_client *c, char *chan_name);

/*
** server_who.c
*/
void					cmd_who(t_client *c);

/*
** server_send.c
*/
void					irc_send_dup(t_client *e, t_client *r, char *data);
size_t					send_msg(t_client *c, t_cbuff *cbuf, size_t size);

/*
** server_event.c
*/
void					event_connect(t_client *c);

/*
** server_error.c
*/
int						ft_usage(char *prog_name);
int						check_error(t_net *net, t_port *server);

/*
** server_newnick.c
*/
void					irc_msg_newnick(t_client *c, char *nickname);

/*
** server_newnick.c
*/
void					event_stdin(t_net *net, char *buffer, size_t size);

#endif
