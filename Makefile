# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: garm <garm@student.42.fr>                  +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2013/08/26 23:49:31 by garm              #+#    #+#              #
##   Updated: 2014/12/08 21:50:26 by garm             ###   ########.fr       ##
#                                                                              #
# **************************************************************************** #

OUTPUTS = client serveur
CLEAN_LIB ?= 1

IRC_PORT = 1337
EXPOSED_PORTS = $(IRC_PORT)

client_ARGS = 127.0.0.1 $(IRC_PORT)
serveur_ARGS = -v $(IRC_PORT)

client_SRC_DIR = src/client
serveur_SRC_DIR = src/serveur

BIN_DIR = .
EXTRA_DIR = extra

client_CFLAGS = -Wall -Wextra -Werror -std=c89
serveur_CFLAGS = $(client_CFLAGS)

client_LDLIBS = -lncurses
serveur_LDLIBS = $(client_LDLIBS)

client_LIBS = libft libftdata libftsock
serveur_LIBS = libft libftdata libftsock

client_SRCS = \
		  		client.c \
		  		client_gui.c \
		  		client_prompt.c \
		  		client_chatbox.c \
		  		client_chatbox_move.c \
		  		client_line.c \
		  		client_keys.c \
				client_show.c \
				client_parser.c \
				client_parser_extra.c \
				client_msg.c \
				client_chan.c \
				client_print.c \
				client_utils.c \
				client_send.c \
				client_event.c \

serveur_SRCS = \
		  		server.c \
		  		server_error.c \
		  		server_msg.c \
		  		server_parser.c \
		  		server_execute.c \
		  		server_nick.c \
		  		server_newnick.c \
		  		server_join.c \
		  		server_leave.c \
		  		server_channel.c \
		  		server_who.c \
		  		server_send.c \
		  		server_event.c \
		  		server_stdin.c \


include Meta.mk

norme:
	norminette src
	norminette include
	norminette extra/libft/src
	norminette extra/libft/include/
	norminette extra/libftdata/src/
	norminette extra/libftdata/include/
	norminette extra/libftsock/src
	norminette extra/libftsock/include/

